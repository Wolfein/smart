<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "loyalty_cards".
 *
 * @property int $id
 * @property string|null $name Название
 * @property float|null $buyout Выкуп, %
 * @property float|null $extension Продление, %
 * @property float|null $selected Добор, %
 * @property float|null $part Частичный выкуп, %
 * @property float|null $pay_acs Скидка на покупку акссесуаров, %
 * @property float|null $real Скидка на % с реализации товара, %
 * @property float|null $cash КЭШБЕК, %
 * @property float|null $pay Скидка при покупке товаров, %
 * @property float|null $pay_real Скидка при покупке товаров, принятых на реализацию, %
 */
class LoyaltyCards extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loyalty_cards';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['buyout', 'extension', 'selected', 'part', 'pay_acs', 'real', 'cash', 'pay', 'pay_real'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'buyout' => 'Выкуп, %',
            'extension' => 'Продление, %',
            'selected' => 'Добор, %',
            'part' => 'Частичный выкуп, %',
            'pay_acs' => 'Скидка на покупку акссесуаров, %',
            'real' => 'Скидка на % с реализации товара, %',
            'cash' => 'КЭШБЕК, %',
            'pay' => 'Скидка при покупке товаров, %',
            'pay_real' => 'Скидка при покупке товаров, принятых на реализацию, %',
        ];
    }
}
