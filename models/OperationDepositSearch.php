<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OperationDeposit;

/**
 * OperationDepositSearch represents the model behind the search form about `app\models\OperationDeposit`.
 */
class OperationDepositSearch extends OperationDeposit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'r_s_id', 'term_deposit', 'tariff_id', 'channels_id'], 'integer'],
            [['ammount'], 'number'],
            [['comment', 'create_at', 'user_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OperationDeposit::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ammount' => $this->ammount,
            'r_s_id' => $this->r_s_id,
            'term_deposit' => $this->term_deposit,
            'tariff_id' => $this->tariff_id,
            'channels_id' => $this->channels_id,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'create_at', $this->create_at])
            ->andFilterWhere(['like', 'user_id', $this->user_id]);

        return $dataProvider;
    }
}
