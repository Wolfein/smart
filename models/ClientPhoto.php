<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_photo".
 *
 * @property int $id
 * @property int $client_id Cвязь с клиентом
 * @property string $link Тип документа
 * @property string $create_at Создан
 */
class ClientPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'integer'],
            [['link', 'create_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Cвязь с клиентом',
            'link' => 'Тип документа',
            'create_at' => 'Создан',
        ];
    }
}
