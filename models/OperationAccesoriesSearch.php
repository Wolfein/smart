<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OperationAccesories;

/**
 * OperationAccesoriesSearch represents the model behind the search form about `app\models\OperationAccesories`.
 */
class OperationAccesoriesSearch extends OperationAccesories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cat_id', 'count', 'r_s_id'], 'integer'],
            [['name', 'datetime_'], 'safe'],
            [['ammount_purchase', 'ammount_sell'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OperationAccesories::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'cat_id' => $this->cat_id,
            'count' => $this->count,
            'ammount_purchase' => $this->ammount_purchase,
            'r_s_id' => $this->r_s_id,
            'ammount_sell' => $this->ammount_sell,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'datetime_', $this->datetime_]);

        return $dataProvider;
    }
}
