<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "accessories".
 *
 * @property int $id
 * @property string $name Название
 * @property int $eye Показ
 * @property int $accessories_id Родительская категория
 */
class Accessories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accessories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['accessories_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'accessories_id' => 'Родительская категория',
        ];
    }


    /**
     * @return array
     */
    public function getListAccessories(){
        return ArrayHelper::map(Accessories::find()->all(), 'id', 'name');
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessories()
    {
        return $this->hasOne(Accessories::className(), ['id' => 'accessories_id']);
    }
}
