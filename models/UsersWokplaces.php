<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users_wokplaces".
 *
 * @property int $id
 * @property int $user_id Связь с пользователем
 * @property int $wokplaces_id Связь с филиалом
 */
class UsersWokplaces extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_wokplaces';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'wokplaces_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Связь с пользователем',
            'wokplaces_id' => 'Связь с филиалом',
        ];
    }
}
