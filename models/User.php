<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login Логин
 * @property string|null $name ФИО
 * @property string|null $photo Фото
 * @property string|null $phone Телефон
 * @property string|null $inn ИНН
 * @property string|null $action_info Действующего на основании
 * @property string|null $comment Комментарий
 * @property string|null $wokplace Комментарий
 * @property int|null $role Роль
 * @property int|null $status Статус
 * @property string $password_hash Зашифрованный пароль
 * @property int $is_deletable Можно удалить или нельзя
 *
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    const ROLE_ADMIN = 0;
    const ROLE_PRODUCTION = 1;

    public $password;
    public $file;
    public $wokplace;

    private $oldPasswordHash;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['name', 'login','phone','name', 'is_deletable', 'wokplace', 'password', 'password_hash', 'role'],
            self::SCENARIO_EDIT => ['name','phone','name', 'login', 'is_deletable', 'wokplace','password', 'password_hash', 'role'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'role',  'phone','role'], 'required'],
            [['login'], 'unique'],
            [['is_deletable', 'role'], 'integer'],
            [['wokplace'], 'safe'],
            [['file','phone'], 'safe'],
            ['login', 'email'],
            [['login', 'password_hash', 'password', 'name'], 'string', 'max' => 255],
        ];
    }

    public function isAdmin()
    {
        return $this->role === self::ROLE_ADMIN;
    }

    public function isProduction()
    {
        return $this->role === self::ROLE_PRODUCTION;
    }

    public function sendEmailMessage($subject, $html)
    {
        Yii::$app->mailer->compose()
            ->setFrom('komfort.notifications@yandex.ru')
            ->setTo($this->login)
            ->setSubject($subject)
            ->setHtmlBody($html)
            ->send();
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        $uid = Yii::$app->user->identity->id;

        if($uid == $this->id)
        {
            Yii::$app->session->setFlash('error', "Вы авторизованы под пользователем «{$this->login}». Удаление невозможно!");
            return false;
        }

        if($this->is_deletable == false)
        {
            Yii::$app->session->setFlash('error', "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }

    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->file != null){
            $fileName = Yii::$app->security->generateRandomString();
            if(is_dir('upload') == false){
                mkdir('upload');
            }
            $path = "upload/{$fileName}.{$this->file->extension}";
            $this->file->saveAs($path);
            if($this->photo != null && file_exists($this->photo)){
                unlink($this->photo);
            }
            $this->photo = $path;
        }

        if (parent::beforeSave($insert)) {

            if($this->password != null){
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }

            return true;
        }


        return false;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Почта',
            'name' => 'ФИО',
            'photo' => 'Фото',
            'phone' => 'Телефон',
            'inn' => 'ИНН',
            'action_info' => 'Действующего на основании',
            'comment' => 'Комментарий',
            'role' => 'Роль',
            'status' => 'Доступ',
            'password_hash' => 'Password Hash',
            'is_deletable' => 'Is Deletable',
            'file' => 'Фото',
            'wokplace' => 'Доступные филиалы',
        ];
    }



    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->wokplace != null) {
            $allSubs = UsersWokplaces::find()->where(['user_id' => $this->id])->all();
            foreach ($allSubs as $sub){
                if (array_search($sub, $this->wokplace) !== false){
                    continue;
                }else{
                    $sub->delete();
                }
            }
            foreach ($this->wokplace as $subgroup) {
                $subgroup2 = UsersWokplaces::find()->where(['user_id' => $this->id, 'wokplaces_id' => $subgroup])->one();
                if (!$subgroup2) {
                    (new UsersWokplaces([
                        'wokplaces_id' => $subgroup,
                        'user_id' => $this->id
                    ]))->save(false);
                }
            }

        }
    }
    /**
     * @return string
     */
    public function getRealImg()
    {
        if($this->photo == null){
            return '/img/noimage.png';
        }

        return $this->photo;
    }

    /**
     * @inheritdoc
     */
    public static function roleLabels()
    {
        return [
            self::ROLE_ADMIN => 'Клиент',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['user_creator_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getPermmission()
    {
        return $this->stat_indet;
    }


    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }


    public function isSuperAdmin()
    {
        return $this->id == 1;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role']);
    }

    /**
     * @return array
     */
    public function getListRole(){
        return ArrayHelper::map(Role::find()->all(), 'id', 'name');
    }
}
