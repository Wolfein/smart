<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Role;

/**
 * RoleSearch represents the model behind the search form about `app\models\Role`.
 */
class RoleSearch extends Role
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'l_1', 'l_2', 'l_3', 'l_4', 'l_5', 'l_6', 'c_1', 'c_2', 'c_3', 'c_4', 'c_5', 'c_6', 'r_1', 'r_2', 'a_1', 'a_2', 'a_3', 'o_1', 'o_2', 'o_3', 'o_4', 'o_5', 'o_6', 'o_7', 'o_8', 'o_9', 'o_10', 's_1', 's_2', 's_3', 's_4', 's_5', 's_6', 's_7', 's_8', 's_9', 's_10', 's_11', 's_12', 's_13', 's_14', 's_15', 's_16', 's_17', 's_18', 's_19', 't_1', 'k_1', 'k_2', 'k_3', 'k_4', 'k_5', 'k_6', 'k_7', 'k_8', 'k_9', 'an_1', 'an_2', 'rep_1', 'rep_2', 'rep_3', 'rep_4', 'rep_5', 'rep_6', 'rep_7', 'cl_1', 'cl_2', 'cl_3'], 'integer'],
            [['name', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Role::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'l_1' => $this->l_1,
            'l_2' => $this->l_2,
            'l_3' => $this->l_3,
            'l_4' => $this->l_4,
            'l_5' => $this->l_5,
            'l_6' => $this->l_6,
            'c_1' => $this->c_1,
            'c_2' => $this->c_2,
            'c_3' => $this->c_3,
            'c_4' => $this->c_4,
            'c_5' => $this->c_5,
            'c_6' => $this->c_6,
            'r_1' => $this->r_1,
            'r_2' => $this->r_2,
            'a_1' => $this->a_1,
            'a_2' => $this->a_2,
            'a_3' => $this->a_3,
            'o_1' => $this->o_1,
            'o_2' => $this->o_2,
            'o_3' => $this->o_3,
            'o_4' => $this->o_4,
            'o_5' => $this->o_5,
            'o_6' => $this->o_6,
            'o_7' => $this->o_7,
            'o_8' => $this->o_8,
            'o_9' => $this->o_9,
            'o_10' => $this->o_10,
            's_1' => $this->s_1,
            's_2' => $this->s_2,
            's_3' => $this->s_3,
            's_4' => $this->s_4,
            's_5' => $this->s_5,
            's_6' => $this->s_6,
            's_7' => $this->s_7,
            's_8' => $this->s_8,
            's_9' => $this->s_9,
            's_10' => $this->s_10,
            's_11' => $this->s_11,
            's_12' => $this->s_12,
            's_13' => $this->s_13,
            's_14' => $this->s_14,
            's_15' => $this->s_15,
            's_16' => $this->s_16,
            's_17' => $this->s_17,
            's_18' => $this->s_18,
            's_19' => $this->s_19,
            't_1' => $this->t_1,
            'k_1' => $this->k_1,
            'k_2' => $this->k_2,
            'k_3' => $this->k_3,
            'k_4' => $this->k_4,
            'k_5' => $this->k_5,
            'k_6' => $this->k_6,
            'k_7' => $this->k_7,
            'k_8' => $this->k_8,
            'k_9' => $this->k_9,
            'an_1' => $this->an_1,
            'an_2' => $this->an_2,
            'rep_1' => $this->rep_1,
            'rep_2' => $this->rep_2,
            'rep_3' => $this->rep_3,
            'rep_4' => $this->rep_4,
            'rep_5' => $this->rep_5,
            'rep_6' => $this->rep_6,
            'rep_7' => $this->rep_7,
            'cl_1' => $this->cl_1,
            'cl_2' => $this->cl_2,
            'cl_3' => $this->cl_3,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
