<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "repairs".
 *
 * @property int $id
 * @property string $name ФИО
 * @property string $phone Телефон
 * @property string $doc Документ
 * @property string $doc_info Серия и номер
 * @property string $doc_who Кем выдан
 * @property string $doc_date Дата выдачи
 */
class Repairs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'repairs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'doc', 'doc_info', 'doc_who', 'doc_date'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'doc' => 'Документ',
            'doc_info' => 'Серия и номер',
            'doc_who' => 'Кем выдан',
            'doc_date' => 'Дата выдачи',
        ];
    }
}
