<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tariff_grace".
 *
 * @property int $id
 * @property int $tariff_id Связь тарифом
 * @property string $type Тип расчета
 * @property int $value Процент
 */
class TariffGrace extends \yii\db\ActiveRecord
{
    public $bank;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff_grace';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank'], 'safe'],
            [['tariff_id', 'value'], 'integer'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Связь тарифом',
            'type' => 'Тип расчета',
            'value' => 'Процент',
            'bank' => 'Процент',
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


        $bankAll = TariffGraceSummit::find()->where(['tariff_id' => $this->id,])->all();
        foreach ($bankAll as $item1) {
            $a = false;
            if ($this->bank == null) {
                $item1->delete();
                continue;
            }
            foreach ($this->bank as $item3) {
                if ($item3['id'] == $item1->id) {
                    $a = true;
                }
                if (!$a) {
                    $item1->delete();
                    break;
                }
            }
        }
        if($this->bank != null){
            foreach ($this->bank as $item) {
                $bank = TariffGraceSummit::find()->where(['id' => $item['id']])->one();
                if (!$bank) {
                    (new TariffGraceSummit([
                        'tariff_id' => $this->id,
                        'day' => $item['day'],
                        'value' => $item['value'],
                    ]))->save(false);
                } else {
                    $bank->day = $item['day'];
                    $bank->value = $item['value'];
                    $bank->save(false);
                }
            }
        }
    }
}
