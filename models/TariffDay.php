<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tariff_day".
 *
 * @property int $id
 * @property int|null $tariff_id Связь тарифом
 * @property int|null $start Максимальный срок займа, дней
 * @property int|null $end Минимальное количество дней
 */
class TariffDay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tariff_day';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tariff_id', 'start', 'end'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Связь тарифом',
            'start' => 'Максимальный срок займа, дней',
            'end' => 'Минимальное количество дней',
        ];
    }
}
