<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "wokplaces".
 *
 * @property int $id
 * @property string $name Название
 * @property int $city_id Город
 * @property string $address Адреес
 * @property string $phone Телефон
 * @property string $type_activity Вариант ведения деятельности
 * @property int $entities_id Этот филиал работает через
 * @property string $photo Фото
 * @property string $ser Серия из настройкки печати
 * @property int $number Номер следующего З/Б
 */
class Wokplaces extends \yii\db\ActiveRecord
{

    public $phone;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wokplaces';
    }

    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'safe'],
            [['city_id', 'entities_id'], 'integer'],
            [['name', 'address', 'type_activity', 'photo', 'ser', 'number'], 'string', 'max' => 255],
            ['file', 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'city_id' => 'Город',
            'address' => 'Адреес',
            'phone' => 'Телефон',
            'type_activity' => 'Вариант ведения деятельности',
            'entities_id' => 'Этот филиал работает через',
            'photo' => 'Фото',
            'ser' => 'Серия из настройкки печати',
            'number' => 'Номер следующего З/Б',
            'file' => 'Аватарка'
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->file != null){
            $fileName = Yii::$app->security->generateRandomString();
            if(is_dir('upload') == false){
                mkdir('upload');
            }
            $path = "upload/{$fileName}.{$this->file->extension}";
            $this->file->saveAs($path);
            if($this->photo != null && file_exists($this->photo)){
                unlink($this->photo);
            }
            $this->photo = $path;
        }
        return parent::beforeSave($insert);
    }


    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


        $bankAll = PhoneWokplaces::find()->where(['wokplaces_id' => $this->id,])->all();
        foreach ($bankAll as $item1) {
            $a = false;
            if ($this->phone == null) {
                $item1->delete();
                continue;
            }
            foreach ($this->phone as $item3) {
                if ($item3['id'] == $item1->id) {
                    $a = true;
                }
                if (!$a) {
                    $item1->delete();
                    break;
                }
            }
        }
        if($this->phone != null){
            foreach ($this->phone as $item) {
                $bank = PhoneWokplaces::find()->where(['id' => $item['id']])->one();
                if (!$bank) {
                    (new PhoneWokplaces([
                        'wokplaces_id' => $this->id,
                        'phone' => $item['phone'],
                    ]))->save(false);
                } else {
                    $bank->phone = $item['phone'];
                    $bank->save(false);
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getRealImg()
    {
        if($this->photo == null){
            return '/img/noimage.png';
        }

        return $this->photo;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return array
     */
    public function getListCity(){
        return ArrayHelper::map(City::find()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntities()
    {
        return $this->hasOne(Entities::className(), ['id' => 'entities_id']);
    }

    /**
     * @return array
     */
    public function getListEntities(){
        return ArrayHelper::map(Entities::find()->all(), 'id', 'name');
    }
}
