<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClientsRating;

/**
 * ClientsRatingSearch represents the model behind the search form about `app\models\ClientsRating`.
 */
class ClientsRatingSearch extends ClientsRating
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['deposit', 'pay', 'pay_real', 'self', 'outpay', 'reserve'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientsRating::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'deposit' => $this->deposit,
            'pay' => $this->pay,
            'pay_real' => $this->pay_real,
            'self' => $this->self,
            'outpay' => $this->outpay,
            'reserve' => $this->reserve,
        ]);

        return $dataProvider;
    }
}
