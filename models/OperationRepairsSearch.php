<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OperationRepairs;

/**
 * OperationRepairsSearch represents the model behind the search form about `app\models\OperationRepairs`.
 */
class OperationRepairsSearch extends OperationRepairs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'master_id'], 'integer'],
            [['name', 'sn', 'defect', 'turn_device', 'datetime_', 'user_id', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OperationRepairs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'master_id' => $this->master_id,
            'datetime_' => $this->datetime_,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'sn', $this->sn])
            ->andFilterWhere(['like', 'defect', $this->defect])
            ->andFilterWhere(['like', 'turn_device', $this->turn_device])
            ->andFilterWhere(['like', 'user_id', $this->user_id])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
