<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "operation_accesories".
 *
 * @property int $id
 * @property int|null $cat_id Категория
 * @property string|null $name Наименование
 * @property int|null $count Количество
 * @property float|null $ammount_purchase Цена покупки 1
 * @property int|null $turn_device Касса / Расчетный счет
 * @property float|null $ammount_sell Цена продажи 1
 * @property string|null $datetime_ Дата/время операции
 */
class OperationAccesories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operation_accesories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_id', 'count', 'r_s_id'], 'integer'],
            [['ammount_purchase', 'ammount_sell'], 'number'],
            [['name', 'datetime_'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cat_id' => 'Категория',
            'name' => 'Наименование',
            'count' => 'Количество',
            'ammount_purchase' => 'Цена покупки (за 1 штуку), руб.',
            'r_s_id' => 'Касса / Расчетный счет',
            'ammount_sell' => 'Цена продажи (за 1 штуку), руб. ',
            'datetime_' => 'Дата/время операции',
        ];
    }

    public function getListAccessories(){
        return ArrayHelper::map(Accessories::find()->all(), 'id', 'name');
    }

    public function getListCategory(){
        return ArrayHelper::map(Categories::find()->all(), 'id', 'name');
    }

    public function getCategory(){
        return $this->hasOne(Categories::className(), ['id' => 'cat_id']);
    }
}
