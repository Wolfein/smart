<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "pawn_print".
 *
 * @property int $id
 * @property int $entities_id Связь реквитов
 * @property string $type_print Вариант печати
 * @property string $type_number Вариант нумерации
 */
class PawnPrint extends \yii\db\ActiveRecord
{

    public $bank;
    public $ser;
    public $number;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pawn_print';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank','ser', 'number'], 'safe'],
            [['entities_id'], 'integer'],
            [['type_print', 'type_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entities_id' => 'Связь реквитов',
            'type_print' => 'Вариант печати',
            'type_number' => 'Вариант нумерации',
            'bank' => 'bank',
            'number' => 'Номер',
            'ser' => 'Серия',
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


        if ($this->type_number == 'Отдельная') {
            foreach ($this->bank as $item) {
                $bank = Wokplaces::find()->where(['id' => $item['id']])->one();
                $bank->ser = $item['ser'];
                $bank->number = $item['number'];
                $bank->save(false);

            }
        } else {
            Wokplaces::updateAll(['ser' => $this->ser, 'number' => $this->number]);
        }

    }


    /**
     * @return array
     */
    public function getListEntities(){
        return ArrayHelper::map(Entities::find()->all(), 'id', 'name');
    }

    /**
     * @return array
     */
    public function getListSer(){
        return [
            'AA' => 'AA',
        ];
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntities()
    {
        return $this->hasOne(Entities::className(), ['id' => 'entities_id']);
    }

}
