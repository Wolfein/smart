<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LoyaltyCards;

/**
 * LoyaltyCardsSearch represents the model behind the search form about `app\models\LoyaltyCards`.
 */
class LoyaltyCardsSearch extends LoyaltyCards
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'safe'],
            [['buyout', 'extension', 'selected', 'part', 'pay_acs', 'real', 'cash', 'pay', 'pay_real'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoyaltyCards::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'buyout' => $this->buyout,
            'extension' => $this->extension,
            'selected' => $this->selected,
            'part' => $this->part,
            'pay_acs' => $this->pay_acs,
            'real' => $this->real,
            'cash' => $this->cash,
            'pay' => $this->pay,
            'pay_real' => $this->pay_real,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
