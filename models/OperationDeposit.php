<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "operation_deposit".
 *
 * @property int $id
 * @property float|null $ammount
 * @property int|null $r_s_id Касса / Расчетный счет
 * @property int|null $term_deposit Срок займа, дней
 * @property int|null $tariff_id Тариф
 * @property int|null $channels_id По какой рекламе пришли
 * @property string|null $comment Комментарий
 * @property string|null $create_at Создан
 * @property string|null $user_id Кто создал
 */
class OperationDeposit extends \yii\db\ActiveRecord
{
    public $subjects;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operation_deposit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subjects'], 'safe'],
            [['ammount'], 'number'],
            [['r_s_id', 'term_deposit', 'tariff_id', 'channels_id'], 'integer'],
            [['comment'], 'string'],
            [['create_at', 'user_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ammount' => 'Сумма займа, руб.',
            'r_s_id' => 'Касса / Расчетный счет',
            'term_deposit' => 'Срок займа, дней',
            'tariff_id' => 'Тариф',
            'channels_id' => 'По какой рекламе пришли',
            'comment' => 'Комментарий',
            'create_at' => 'Дата/время операции',
            'status' => 'Статус',
            'user_id' => 'Кем создан',
        ];
    }

    public function getListCategory(){
        return ArrayHelper::map(Categories::find()->all(), 'id', 'name');
    }

    public function getListStorage(){
        return ArrayHelper::map(Storage::find()->all(), 'id', 'name');
    }
    public function getListSubject(){
        return ArrayHelper::map(OperationDepositSubject::find()->andWhere(['=','deposit_id',$this->id])->all(), 'id', 'name');
    }

    public function getListTarif(){
        return ArrayHelper::map(Tariff::find()->all(), 'id', 'name');
    }

    public function getTarif(){
        return $this->hasOne(Tariff::className(), ['id' => 'tariff_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $SubjectAll = OperationDepositSubject::find()->where(['deposit_id' => $this->id,])->all();
        foreach ($SubjectAll as $item1) {
            $a = false;
            if ($this->subjects == null) {
                $item1->delete();
                continue;
            }
            foreach ($this->subjects as $item3) {
                if ($item3['id'] == $item1->id) {
                    $a = true;
                }
                if (!$a) {
                    $item1->delete();
                    break;
                }
            }
        }
        if($this->subjects != null){
            foreach ($this->subjects as $item) {
                $subject = OperationDepositSubject::find()->where(['id' => $item['id']])->one();
                if (!$subject) {
                    $m = new OperationDepositSubject();
                    (new OperationDepositSubject([
                        'deposit_id' => $this->id,
                        'cat_id' => $item['cat_id'],
                        'sub_cat_id' => $item['sub_cat_id'],
                        'name' => $item['name'],
                        'storage_id' => $item['storage_id'],
                        'description' => $item['description'],
                        'comment' => $item['comment'],
                    ]))->save(false);

                } else {
                    $subject->cat_id = $item['cat_id'];
                    $subject->sub_cat_id = $item['sub_cat_id'];
                    $subject->name = $item['name'];
                    $subject->storage_id = $item['storage_id'];
                    $subject->description = $item['description'];
                    $subject->comment = $item['comment'];
                    $subject->save(false);
                }
            }
        }
    }

}
