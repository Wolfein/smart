<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string|null $surname Фамилия
 * @property string|null $name Имя
 * @property string|null $middle Отчество
 * @property string|null $birthday Дата рождения
 * @property string|null $city Нас. пункт
 * @property string|null $street Улица
 * @property string|null $home Дом
 * @property string|null $room Квартира
 * @property string|null $actual_address Фактический адрес проживания
 * @property string|null $place_birth Место рождения
 * @property string|null $phone Контактный телефон
 * @property int|null $channels_id Откуда о нас узнали
 * @property int|null $cards_id Карта
 * @property string|null $email Электронная почта
 * @property string|null $nationality Гражданство
 * @property string|null $inn ИНН
 * @property string|null $snils СНИЛС
 * @property string|null $pattern Характеристика клиента
 * @property string|null $info Предупреждение
 * @property string|null $create_at Создан
 * @property string|null $user_id Кто создал
 */
class Client extends \yii\db\ActiveRecord
{

    public $doc;
    public $files;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birthday','doc','files'], 'safe'],
            [['channels_id', 'cards_id'], 'integer'],
            [['pattern', 'info'], 'string'],
            [['surname', 'name', 'middle', 'city', 'street', 'home', 'room', 'actual_address', 'place_birth', 'phone', 'email', 'nationality', 'inn', 'snils', 'create_at', 'user_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'middle' => 'Отчество',
            'birthday' => 'Дата рождения',
            'city' => 'Нас. пункт',
            'street' => 'Улица',
            'home' => 'Дом',
            'room' => 'Квартира',
            'actual_address' => 'Фактический адрес проживания',
            'place_birth' => 'Место рождения',
            'phone' => 'Контактный телефон',
            'channels_id' => 'Откуда о нас узнали',
            'cards_id' => 'Карта',
            'email' => 'Электронная почта',
            'nationality' => 'Гражданство',
            'inn' => 'ИНН',
            'snils' => 'СНИЛС',
            'pattern' => 'Характеристика клиента',
            'info' => 'Предупреждение',
            'create_at' => 'Создан',
            'user_id' => 'Кто создал',
            'doc' => 'Документы',
            'files' => 'Документы',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


        $bankAll = ClientDoc::find()->where(['client_id' => $this->id,])->all();
        foreach ($bankAll as $item1) {
            $a = false;
            if ($this->doc == null) {
                $item1->delete();
                continue;
            }
            foreach ($this->doc as $item3) {
                if ($item3['id'] == $item1->id) {
                    $a = true;
                }
                if (!$a) {
                    $item1->delete();
                    break;
                }
            }
        }
        if($this->doc != null){
            foreach ($this->doc as $item) {
                $bank = ClientDoc::find()->where(['id' => $item['id']])->one();
                if (!$bank) {
                    (new ClientDoc([
                        'client_id' => $this->id,
                        'type' => $item['type'],
                        'number' => $item['number'],
                        'who' => $item['who'],
                        'date' => $item['date'],
                        'code' => $item['code'],
                        'create_at' => date('Y-m-d H:i:s'),
                    ]))->save(false);
                } else {
                    $bank->type = $item['type'];
                    $bank->number = $item['number'];
                    $bank->who = $item['who'];
                    $bank->date = $item['date'];
                    $bank->code = $item['code'];
                    $bank->save(false);
                }
            }
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChanel()
    {
        return $this->hasOne(AdvertisingChannels::className(), ['id' => 'channels_id']);
    }

    /**
     * @return array
     */
    public function getListChanel(){
        return ArrayHelper::map(AdvertisingChannels::find()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCards()
    {
        return $this->hasOne(LoyaltyCards::className(), ['id' => 'cards_id']);
    }

    /**
     * @return array
     */
    public function getListCard(){
        return ArrayHelper::map(LoyaltyCards::find()->all(), 'id', 'name');
    }


    public function getFullName()
    {
        return $this->surname.' '.$this->name.' '.$this->middle;
    }

    public function getListClient(){
        return ArrayHelper::map(Client::find()->all(), 'id', 'FullName');
    }
}
