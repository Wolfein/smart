<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TariffGraceSummit;

/**
 * TariffGraceSummitSearch represents the model behind the search form about `app\models\TariffGraceSummit`.
 */
class TariffGraceSummitSearch extends TariffGraceSummit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tariff_id', 'day'], 'integer'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TariffGraceSummit::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tariff_id' => $this->tariff_id,
            'day' => $this->day,
            'value' => $this->value,
        ]);

        return $dataProvider;
    }
}
