<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clients_rating".
 *
 * @property int $id
 * @property double $deposit Залог (только первичный, перезалоги не учитываются)
 * @property double $pay Скупка
 * @property double $pay_real Прием на реализацию
 * @property double $self Продажа клиенту
 * @property double $outpay Не выкуп (вывод из залога)
 * @property double $reserve Изъятие (за каждую изъятую вещь)
 */
class ClientsRating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients_rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['deposit', 'pay', 'pay_real', 'self', 'outpay', 'reserve'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deposit' => 'Залог (только первичный, перезалоги не учитываются)',
            'pay' => 'Скупка',
            'pay_real' => 'Прием на реализацию',
            'self' => 'Продажа клиенту',
            'outpay' => 'Не выкуп (вывод из залога)',
            'reserve' => 'Изъятие (за каждую изъятую вещь)',
        ];
    }
}
