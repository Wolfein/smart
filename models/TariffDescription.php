<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tariff_description".
 *
 * @property int $id
 * @property int $tariff_id Связь тарифом
 * @property string $loan Процентная ставка по займу
 * @property double $old На прочие услуги
 */
class TariffDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id'], 'integer'],
            [['old'], 'string'],
            [['loan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Связь тарифом',
            'loan' => 'Процентная ставка по займу',
            'old' => 'На прочие услуги',
        ];
    }
}
