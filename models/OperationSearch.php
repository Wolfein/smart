<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Operation;

/**
 * OperationSearch represents the model behind the search form about `app\models\Operation`.
 */
class OperationSearch extends Operation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'client_doc_id', 'categories_1_id', 'categories_2_id', 'r_s_id', 'channels_id'], 'integer'],
            [['type', 'name', 'number', 'markdows', 'info', 'comment', 'create_at', 'user_id'], 'safe'],
            [['sum_pay', 'sum_out', 'comis'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Operation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'client_doc_id' => $this->client_doc_id,
            'categories_1_id' => $this->categories_1_id,
            'categories_2_id' => $this->categories_2_id,
            'sum_pay' => $this->sum_pay,
            'sum_out' => $this->sum_out,
            'comis' => $this->comis,
            'r_s_id' => $this->r_s_id,
            'channels_id' => $this->channels_id,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'markdows', $this->markdows])
            ->andFilterWhere(['like', 'info', $this->info])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'create_at', $this->create_at])
            ->andFilterWhere(['like', 'user_id', $this->user_id]);

        return $dataProvider;
    }
}
