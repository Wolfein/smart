<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tariff_grace_summit".
 *
 * @property int $id
 * @property int $tariff_id Связь тарифом
 * @property int $day Интервал дней
 * @property double $value Процент
 */
class TariffGraceSummit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff_grace_summit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'day'], 'integer'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Связь тарифом',
            'day' => 'Интервал дней',
            'value' => 'Процент',
        ];
    }
}
