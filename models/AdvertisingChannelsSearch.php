<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AdvertisingChannels;

/**
 * AdvertisingChannelsSearch represents the model behind the search form about `app\models\AdvertisingChannels`.
 */
class AdvertisingChannelsSearch extends AdvertisingChannels
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'add_cl', 'dep', 'pay', 'pay_self', 'self', 'self_acs', 'add_rem'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdvertisingChannels::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'add_cl', $this->add_cl])
            ->andFilterWhere(['like', 'dep', $this->dep])
            ->andFilterWhere(['like', 'pay', $this->pay])
            ->andFilterWhere(['like', 'pay_self', $this->pay_self])
            ->andFilterWhere(['like', 'self', $this->self])
            ->andFilterWhere(['like', 'self_acs', $this->self_acs])
            ->andFilterWhere(['like', 'add_rem', $this->add_rem]);

        return $dataProvider;
    }
}
