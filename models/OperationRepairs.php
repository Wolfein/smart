<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "operation_repairs".
 *
 * @property int $id
 * @property string|null $name Наименоание
 * @property string|null $sn Серийный номер
 * @property string|null $defect Дефект
 * @property int|null $turn_device Аппарат включается?
 * @property int|null $master_id Мастер
 * @property string $datetime_ Дата/время операции
 * @property string|null $user_id Кто создал
 * @property string|null $comment Комментарий
 */
class OperationRepairs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operation_repairs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['turn_device', 'master_id', 'status'], 'integer'],
            [['datetime_'], 'safe'],
            [['comment'], 'string'],
            [['name', 'sn', 'defect', 'user_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'sn' => 'Серийный номер',
            'defect' => 'Неисправность',
            'turn_device' => 'Включается?',
            'master_id' => 'Мастер',
            'datetime_' => 'Дата/время операции',
            'user_id' => 'Кто создал',
            'status' => 'Статус',
            'comment' => 'Комментарий',
        ];
    }

    public function getListMaster(){
        return ArrayHelper::map(Repairs::find()->all(), 'id', 'name');
    }

    public function getMaster(){
        return $this->hasOne(Repairs::className(), ['id' => 'master_id']);
    }


}
