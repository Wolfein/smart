<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tariff_extended".
 *
 * @property int $id
 * @property int $tariff_id Связь тарифом
 * @property double $day Оплата хранения, руб. в день
 * @property double $value Стоимость оценки, руб.
 */
class TariffExtended extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff_extended';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id'], 'integer'],
            [['day', 'value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Связь тарифом',
            'day' => 'Оплата хранения, руб. в день',
            'value' => 'Стоимость оценки, руб.',
        ];
    }
}
