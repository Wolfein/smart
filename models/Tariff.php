<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tariff".
 *
 * @property int $id
 * @property string $name Название
 * @property string $comment Комментарий
 * @property double $sum_min Минимальная сумма
 * @property double $sum_max Минимальная сумма
 * @property string $type_roud Округление суммы оплаты
 * @property int $count_day Количество первых бесплатных дней
 * @property int $actiont Действие карты постоянного клиента
 * @property int $wokplaces_id Филиалы использования тарифа
 */
class Tariff extends \yii\db\ActiveRecord
{
    public $bank;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank'], 'safe'],
            [['sum_min','sum_max'], 'number'],
            [['count_day', 'actiont', 'wokplaces_id'], 'integer'],
            [['name', 'comment', 'type_roud'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'comment' => 'Комментарий',
            'sum_min' => 'Минимальная сумма',
            'sum_max' => 'Максимальная сумма',
            'type_roud' => 'Округление суммы оплаты',
            'count_day' => 'Количество первых бесплатных дней',
            'actiont' => 'Действие карты постоянного клиента',
            'wokplaces_id' => 'Филиалы использования тарифа',
            'bank' => 'bank',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        (new TariffInterval([
            'tariff_id' => $this->id,
            'limit_max' => 0,
            'limit_min' => 0,
            'type_roud' => 'Без округления',
            'days_calc' => 'Обычный расчет',
        ]))->save(false);

        //Описание
        (new TariffDescription([
            'tariff_id' => $this->id,
            'loan' => '',
            'old' => '',
        ]))->save(false);

        //Дополнительныее услуги
        (new TariffExtended([
            'tariff_id' => $this->id,
            'day' => 0,
            'value' => 0,
        ]))->save(false);

        //Просрочка
        (new TariffGrace([
            'tariff_id' => $this->id,
            'type' => '',
            'value' => 0,
        ]))->save(false);

        $bankAll = TariffDay::find()->where(['tariff_id' => $this->id,])->all();
        foreach ($bankAll as $item1) {
            $a = false;
            if ($this->bank == null) {
                $item1->delete();
                continue;
            }
            foreach ($this->bank as $item3) {
                if ($item3['id'] == $item1->id) {
                    $a = true;
                }
                if (!$a) {
                    $item1->delete();
                    break;
                }
            }
        }
        if($this->bank != null){
            foreach ($this->bank as $item) {
                $bank = TariffDay::find()->where(['id' => $item['id']])->one();
                if (!$bank) {
                    (new TariffDay([
                        'tariff_id' => $this->id,
                        'start' => $item['start'],
                        'end' => $item['end'],
                    ]))->save(false);
                } else {
                    $bank->start = $item['start'];
                    $bank->end = $item['end'];
                    $bank->save(false);
                }
            }
        }

    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWokplaces()
    {
        return $this->hasOne(Wokplaces::className(), ['id' => 'wokplaces_id']);
    }

    /**
     * @return array
     */
    public function getListWokplaces(){
        return ArrayHelper::map(Wokplaces::find()->all(), 'id', 'name');
    }
}
