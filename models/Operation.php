<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "operation".
 *
 * @property int $id
 * @property string $type Тип
 * @property int $client_id Клиент
 * @property int $client_doc_id Документ
 * @property int $categories_1_id Категория
 * @property int $categories_2_id Под категория
 * @property string $name Наименование
 * @property string $number Серийный номер
 * @property double $sum_pay Сумма скупки
 * @property double $sum_out Цена продажи, руб.
 * @property double $comis Комиссионное вознаграждение, руб
 * @property int $markdows Разрешить автоматическую уценку
 * @property int $r_s_id Касса / Расчетный счет
 * @property string $info Описание
 * @property int $channels_id По какой рекламе пришли
 * @property string $comment Комментарий
 * @property string $create_at Создан
 * @property string $user_id Кто создал
 */
class Operation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'client_doc_id', 'categories_1_id', 'categories_2_id', 'markdows', 'r_s_id', 'channels_id'], 'integer'],
            [['sum_pay', 'sum_out', 'comis'], 'number'],
            [['info', 'comment'], 'string'],
            [['type', 'name', 'number', 'create_at', 'user_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'client_id' => 'Клиент',
            'client_doc_id' => 'Документ',
            'categories_1_id' => 'Категория',
            'categories_2_id' => 'Под категория',
            'name' => 'Наименование',
            'number' => 'Серийный номер',
            'sum_pay' => 'Сумма скупки',
            'sum_out' => 'Цена продажи, руб.',
            'comis' => 'Комиссионное вознаграждение, руб',
            'markdows' => 'Разрешить автоматическую уценку',
            'r_s_id' => 'Касса / Расчетный счет',
            'info' => 'Описание',
            'channels_id' => 'По какой рекламе пришли',
            'comment' => 'Комментарий',
            'create_at' => 'Создан',
            'user_id' => 'Кто создал',
        ];
    }

    public function getListCategory(){
        return ArrayHelper::map(Categories::find()->all(), 'id', 'name');
    }
    public function getListClient(){
        return ArrayHelper::map(Client::find()->all(), 'id', 'name');
    }


}
