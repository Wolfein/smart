<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EntitiesBank;

/**
 * EntitiesBankSearch represents the model behind the search form about `app\models\EntitiesBank`.
 */
class EntitiesBankSearch extends EntitiesBank
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'entities_id'], 'integer'],
            [['r_s', 'bank', 'k_s', 'bik'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EntitiesBank::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'entities_id' => $this->entities_id,
        ]);

        $query->andFilterWhere(['like', 'r_s', $this->r_s])
            ->andFilterWhere(['like', 'bank', $this->bank])
            ->andFilterWhere(['like', 'k_s', $this->k_s])
            ->andFilterWhere(['like', 'bik', $this->bik]);

        return $dataProvider;
    }
}
