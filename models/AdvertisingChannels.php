<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advertising_channels".
 *
 * @property int $id
 * @property string $name Название
 * @property int $add_cl Добавление клиента
 * @property int $dep Залог
 * @property int $pay Скупка
 * @property int $pay_self Прием на реализацию
 * @property int $self Продажа товара
 * @property int $self_acs Продажа аксессуаров
 * @property int $add_rem Прием на ремонт
 */
class AdvertisingChannels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertising_channels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['add_cl', 'dep', 'pay', 'pay_self', 'self', 'self_acs', 'add_rem'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'add_cl' => 'Добавление клиента',
            'dep' => 'Залог',
            'pay' => 'Скупка',
            'pay_self' => 'Прием на реализацию',
            'self' => 'Продажа товара',
            'self_acs' => 'Продажа аксессуаров',
            'add_rem' => 'Прием на ремонт',
        ];
    }
}
