<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "operation_deposit_subject".
 *
 * @property int $id
 * @property int|null $deposit_id Связь депозит
 * @property int|null $cat_id Категория
 * @property int|null $sub_cat_id Подкатегория
 * @property string|null $name Наименование
 * @property int|null $storage_id Место хранения
 * @property string|null $description Описание
 * @property string|null $comment Комментарий
 */
class OperationDepositSubject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operation_deposit_subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deposit_id', 'cat_id', 'sub_cat_id', 'storage_id', 'status'], 'integer'],
            [['description', 'comment'], 'string'],
            [['ammount'], 'number'],
            [['name','sn','dt_status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deposit_id' => 'Deposit ID',
            'cat_id' => 'Категория',
            'sub_cat_id' => 'Подкатегория',
            'name' => 'Наименование',
            'ammount' => 'Стоимость оценки',
            'sn' => 'Серийный номер',
            'storage_id' => 'Место хранения',
            'description' => 'Описание',
            'comment' => 'Комментарий',
            'status' => 'Статус',
            'dt_status' => 'Дата статуса',
        ];
    }

    public function getListCategory(){
        return ArrayHelper::map(Categories::find()->all(), 'id', 'name');
    }

    public function getCategory(){
        return $this->hasOne(Categories::className(), ['id' => 'cat_id']);
    }

    public function getSubCategory(){
        return $this->hasOne(Categories::className(), ['id' => 'sub_cat_id']);
    }

    public function getListStorage(){
        return ArrayHelper::map(Storage::find()->all(), 'id', 'name');
    }

    public function getStorage(){
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

//    public function getOperation(){
//        return $this->hasOne(OperationDeposit::className(), ['id' => 'deposit_id']);
//    }

}
