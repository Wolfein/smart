<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operation_repairs_photo".
 *
 * @property int $id
 * @property int|null $operation_repairs_id Cвязь
 * @property string|null $link Тип документа
 * @property string $create_at Создан
 */
class OperationRepairsPhoto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operation_repairs_photo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['operation_repairs_id'], 'integer'],
            [['create_at'], 'safe'],
            [['link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'operation_repairs_id' => 'Operation Repairs ID',
            'link' => 'Link',
            'create_at' => 'Create At',
        ];
    }
}
