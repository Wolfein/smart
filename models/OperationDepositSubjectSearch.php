<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OperationDepositSubject;

/**
 * OperationDepositSubjectSearch represents the model behind the search form of `app\models\OperationDepositSubject`.
 */
class OperationDepositSubjectSearch extends OperationDepositSubject
{
    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['id', 'deposit_id', 'cat_id', 'sub_cat_id', 'storage_id', 'status'], 'integer'],
            [['name', 'description', 'comment','sn','dt_status'], 'safe'],
            [['ammount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OperationDepositSubject::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'deposit_id' => $this->deposit_id,
            'cat_id' => $this->cat_id,
            'sub_cat_id' => $this->sub_cat_id,
            'storage_id' => $this->storage_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
