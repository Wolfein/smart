<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TariffDescription;

/**
 * TariffDescriptionSearch represents the model behind the search form about `app\models\TariffDescription`.
 */
class TariffDescriptionSearch extends TariffDescription
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tariff_id'], 'integer'],
            [['loan'], 'safe'],
            [['old'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TariffDescription::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tariff_id' => $this->tariff_id,
            'old' => $this->old,
        ]);

        $query->andFilterWhere(['like', 'loan', $this->loan]);

        return $dataProvider;
    }
}
