<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_doc".
 *
 * @property int $id
 * @property int $client_id Cвязь с клиентом
 * @property string $type Тип документа
 * @property string $number Серия, номер
 * @property string $who Кем выдан
 * @property string $date Дата выдачи
 * @property string $code Код подразделения
 * @property string $create_at Создан
 */
class ClientDoc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_doc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'integer'],
            [['date', 'code'], 'safe'],
            [['type', 'number', 'who', 'create_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Cвязь с клиентом',
            'type' => 'Тип документа',
            'number' => 'Серия, номер',
            'who' => 'Кем выдан',
            'date' => 'Дата выдачи',
            'code' => 'Код подразделения',
            'create_at' => 'Создан',
        ];
    }
}
