<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entities_bank".
 *
 * @property int $id
 * @property int $entities_id Связь
 * @property int $eye Показ
 * @property string $r_s р/с
 * @property string $bank Банк
 * @property string $k_s к/с
 * @property string $bik БИК
 */
class EntitiesBank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entities_bank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entities_id'], 'integer'],
            [['r_s', 'bank', 'k_s', 'bik'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entities_id' => 'Связь',
            'r_s' => 'р/с',
            'bank' => 'Банк',
            'k_s' => 'к/с',
            'bik' => 'БИК',
            'bik' => 'Показ',
        ];
    }
}
