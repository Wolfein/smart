<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operation_photo".
 *
 * @property int $id
 * @property int $operation_id Cвязь
 * @property string $link Тип документа
 * @property string $create_at Создан
 */
class OperationPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operation_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['operation_id'], 'integer'],
            [['link', 'create_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'operation_id' => 'Cвязь',
            'link' => 'Тип документа',
            'create_at' => 'Создан',
        ];
    }
}
