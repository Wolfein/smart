<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $name Название
 * @property int $eye Показ
 * @property int $categories_id Родительская категория
 * @property int $tariff_id Тариф по умолчанию
 * @property string $storage_id Место хранения по умолчанию
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categories_id', 'tariff_id'], 'integer'],
            [['name', 'storage_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'categories_id' => 'Родительская категория',
            'tariff_id' => 'Тариф по умолчанию',
            'storage_id' => 'Место хранения по умолчанию',
        ];
    }



    /**
     * @return array
     */
    public function getListCategories(){
        return ArrayHelper::map(Categories::find()->where(['is','categories_id',null])->all(), 'id', 'name');
    }

    /**
     * @return array
     */
    public function getListStorage(){
        return ArrayHelper::map(Storage::find()->all(), 'id', 'name');
    }

    /**
     * @return array
     */
    public function getListTariff(){
        return ArrayHelper::map(Tariff::find()->all(), 'id', 'name');
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storage::className(), ['id' => 'storage_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasOne(Categories::className(), ['id' => 'categories_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['id' => 'tariff_id']);
    }
}
