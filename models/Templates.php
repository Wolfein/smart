<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "templates".
 *
 * @property int $id
 * @property string $name Название
 * @property string $type Тип комис или ломбард
 * @property string $description Описание
 * @property string $text Текст
 * @property int $status Статус
 * @property int $blank Пустые бланки:
 * @property int $dep Залог
 * @property int $contin продление
 * @property int $contin_on продление (online)
 * @property int $dep_rep перезалог
 * @property int $dobor добор
 * @property int $byuout_rem частичный выкуп
 * @property int $byuout выкуп
 * @property int $out вывод из залога
 */
class Templates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'text'], 'string'],
            [['status', 'blank', 'dep', 'contin', 'contin_on', 'dep_rep', 'dobor', 'byuout_rem', 'byuout', 'out'], 'integer'],
            [['name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'type' => 'Тип комис или ломбард',
            'description' => 'Описание',
            'text' => 'Текст',
            'status' => 'Активный (выводится на странице печати документов)',
            'blank' => 'Выводить пустой бланк для сотрудников',
            'dep' => 'Залог',
            'contin' => 'продление',
            'contin_on' => 'продление (online)',
            'dep_rep' => 'перезалог',
            'dobor' => 'добор',
            'byuout_rem' => 'частичный выкуп',
            'byuout' => 'выкуп',
            'out' => 'вывод из залога',
        ];
    }
}
