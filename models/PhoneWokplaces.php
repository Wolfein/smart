<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "phone_wokplaces".
 *
 * @property int $id
 * @property string|null $phone Телефон
 * @property string|null $wokplaces_id Связь с филлиалом
 */
class PhoneWokplaces extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'phone_wokplaces';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone', 'wokplaces_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Телефон',
            'wokplaces_id' => 'Связь с филлиалом',
        ];
    }
}
