<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Templates;

/**
 * TemplatesSearch represents the model behind the search form about `app\models\Templates`.
 */
class TemplatesSearch extends Templates
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'type', 'description', 'text', 'status', 'blank', 'dep', 'contin', 'contin_on', 'dep_rep', 'dobor', 'byuout_rem', 'byuout', 'out'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Templates::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'blank', $this->blank])
            ->andFilterWhere(['like', 'dep', $this->dep])
            ->andFilterWhere(['like', 'contin', $this->contin])
            ->andFilterWhere(['like', 'contin_on', $this->contin_on])
            ->andFilterWhere(['like', 'dep_rep', $this->dep_rep])
            ->andFilterWhere(['like', 'dobor', $this->dobor])
            ->andFilterWhere(['like', 'byuout_rem', $this->byuout_rem])
            ->andFilterWhere(['like', 'byuout', $this->byuout])
            ->andFilterWhere(['like', 'out', $this->out]);

        return $dataProvider;
    }
}
