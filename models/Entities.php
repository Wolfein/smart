<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entities".
 *
 * @property int $id
 * @property string $name Название юр. лица
 * @property string $address Юридический адрес
 * @property string $phone Общий телефон организации
 * @property string $director Директор
 * @property string $buh Главный бухгалтер
 * @property string $inn ИНН
 * @property string $kpp КПП
 * @property string $ogrn ОГРН
 * @property string $okpo ОКПО
 * @property string $oktmo ОКТМО
 */
class Entities extends \yii\db\ActiveRecord
{


    public $bank;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address', 'phone', 'director', 'buh', 'inn', 'kpp', 'ogrn', 'okpo', 'oktmo'], 'string', 'max' => 255],
            [['bank'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название юр. лица',
            'address' => 'Юридический адрес',
            'phone' => 'Общий телефон организации',
            'director' => 'Директор',
            'buh' => 'Главный бухгалтер',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'ogrn' => 'ОГРН',
            'okpo' => 'ОКПО',
            'oktmo' => 'ОКТМО',
            'bank' => 'entities_bank',
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


            $bankAll = EntitiesBank::find()->where(['entities_id' => $this->id,])->all();
            foreach ($bankAll as $item1) {
                $a = false;
                if ($this->bank == null) {
                    $item1->delete();
                    continue;
                }
                foreach ($this->bank as $item3) {
                    if ($item3['id'] == $item1->id) {
                        $a = true;
                    }
                    if (!$a) {
                        $item1->delete();
                        break;
                    }
                }
            }
        if($this->bank != null){
            foreach ($this->bank as $item) {
                $bank = EntitiesBank::find()->where(['id' => $item['id']])->one();
                if (!$bank) {
                    (new EntitiesBank([
                        'entities_id' => $this->id,
                        'r_s' => $item['r_s'],
                        'bank' => $item['bank'],
                        'k_s' => $item['k_s'],
                        'bik' => $item['bik'],
                        'eye' => $item['eye'],
                    ]))->save(false);
                } else {
                    $bank->r_s = $item['r_s'];
                    $bank->bank = $item['bank'];
                    $bank->k_s = $item['k_s'];
                    $bank->bik = $item['bik'];
                    $bank->eye = $item['eye'];
                    $bank->save(false);
                }
            }
        }
    }
}
