<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tariff_interval".
 *
 * @property int $id
 * @property int $tariff_id Связь тарифом
 * @property int $limit_max Максимальный срок займа, дней
 * @property int $limit_min Минимальное количество дней
 * @property string $type_roud Округление срока залога
 * @property string $days_calc Расчет количества дней
 */
class TariffInterval extends \yii\db\ActiveRecord
{
    public $bank;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff_interval';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank'], 'safe'],
            [['tariff_id', 'limit_max', 'limit_min'], 'integer'],
            [['type_roud', 'days_calc'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Связь тарифом',
            'limit_max' => 'Максимальный срок займа, дней',
            'limit_min' => 'Минимальное количество дней',
            'type_roud' => 'Округление срока залога',
            'days_calc' => 'Расчет количества дней',
            'bank' => 'bank',
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $bankAll = TariffIntervalSummit::find()->where(['tariff_id' => $this->id,])->all();
        foreach ($bankAll as $item1) {
            $a = false;
            if ($this->bank == null) {
                $item1->delete();
                continue;
            }
            foreach ($this->bank as $item3) {
                if ($item3['id'] == $item1->id) {
                    $a = true;
                }
                if (!$a) {
                    $item1->delete();
                    break;
                }
            }
        }
        if($this->bank != null){
            foreach ($this->bank as $item) {
                $bank = TariffIntervalSummit::find()->where(['id' => $item['id']])->one();
                if (!$bank) {
                    (new TariffIntervalSummit([
                        'tariff_id' => $this->id,
                        'day' => $item['day'],
                        'value' => $item['value'],
                    ]))->save(false);
                } else {
                    $bank->day = $item['day'];
                    $bank->value = $item['value'];
                    $bank->save(false);
                }
            }
        }
    }
}
