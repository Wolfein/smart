<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tariff_interval_summit".
 *
 * @property int $id
 * @property int $tariff_id Связь тарифом
 * @property int $day Интервал срок
 * @property int $sum Интервал сумм
 * @property double $value Процент
 */
class TariffIntervalSummit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff_interval_summit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'day', 'sum'], 'integer'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Связь тарифом',
            'day' => 'Интервал срок',
            'sum' => 'Интервал сумм',
            'value' => 'Процент',
        ];
    }
}
