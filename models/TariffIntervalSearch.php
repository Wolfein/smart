<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TariffInterval;

/**
 * TariffIntervalSearch represents the model behind the search form about `app\models\TariffInterval`.
 */
class TariffIntervalSearch extends TariffInterval
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tariff_id', 'limit_max', 'limit_min'], 'integer'],
            [['type_roud', 'days_calc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TariffInterval::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tariff_id' => $this->tariff_id,
            'limit_max' => $this->limit_max,
            'limit_min' => $this->limit_min,
        ]);

        $query->andFilterWhere(['like', 'type_roud', $this->type_roud])
            ->andFilterWhere(['like', 'days_calc', $this->days_calc]);

        return $dataProvider;
    }
}
