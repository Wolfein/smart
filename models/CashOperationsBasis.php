<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cash_operations_basis".
 *
 * @property int $id
 * @property string $name Основание кассовой/банковской операции
 * @property string $type Вид
 * @property string $cas Касса / Расчетный счет
 */
class CashOperationsBasis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cash_operations_basis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'cas'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Основание кассовой/банковской операции',
            'type' => 'Вид',
            'cas' => 'Касса / Расчетный счет',
        ];
    }
}
