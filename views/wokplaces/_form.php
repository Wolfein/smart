<?php

use app\models\PhoneWokplaces;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\Wokplaces */
/* @var $form yii\widgets\ActiveForm */



if($model->isNewRecord == false){
    $model->phone = PhoneWokplaces::find()->where(['wokplaces_id' => $model->id])->asArray()->all();

    if(count($model->phone) > 0){
        $lastId = $model->phone[count($model->phone)-1]['id'];
    }
}
?>

<div class="wokplaces-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_id')->dropDownList($model->getListCity()) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->widget(MultipleInput::className(), [
            'id' => 'my_id',
            'columns' => [
                [
                    'name' => 'id',
                    'title' => '',
                    'options' => [
                        'type' => 'hidden',
                    ]
                ],
                [
                    'name' => 'phone',
                    'title' => 'Телефон',
                ],
            ],
        ])->label(false) ?>

    <?= $form->field($model, 'type_activity')->dropDownList(
            ['Одно юр. лицо для всех видов деятельности' => 'Одно юр. лицо для всех видов деятельности',
                'Разные юр. лица для разных видов деятельности' => 'Разные юр. лица для разных видов деятельности'
                ]) ?>

    <?= $form->field($model, 'entities_id')->dropDownList($model->getListEntities()) ?>

    <?= $form->field($model, 'file')->fileInput() ?>


  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
