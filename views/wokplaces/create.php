<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Wokplaces */

?>
<div class="wokplaces-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
