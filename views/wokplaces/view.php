<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Wokplaces */
?>
<div class="wokplaces-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'city_id',
            'address',
            'type_activity',
            'entities_id',
            'photo',
            'ser',
            'number',
        ],
    ]) ?>

</div>
