<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TariffGrace */
?>
<div class="tariff-grace-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
