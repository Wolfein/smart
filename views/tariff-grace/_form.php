<?php

use app\models\TariffGraceSummit;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\TariffGrace */
/* @var $form yii\widgets\ActiveForm */



if($model->isNewRecord == false){
    $model->bank = TariffGraceSummit::find()->where(['tariff_id' => $model->id])->asArray()->all();

    if(count($model->bank) > 0){
        $lastId = $model->bank[count($model->bank)-1]['id'];
    }
}

?>

<div class="tariff-grace-form">


    <?php $form = ActiveForm::begin(['action' => '/tariff-grace/update?id='.$model->id]); ?>

    <?= $form->field($model, 'value')->textInput() ?>

    <h4>Процентные ставки по просрочке (льготному периоду), % в день</h4>
        <?= $form->field($model, 'bank')->widget(MultipleInput::className(), [
            'id' => 'my_id1',
            'min' => 0,
            'columns' => [
                [
                    'name' => 'id',
                    'options' => [
                        'type' => 'hidden',
                    ]
                ],
                [
                    'name' => 'day',
                    'title' => 'Интервал',
                ],
                [
                    'name' => 'value',
                    'title' => 'Значение',
                ],
            ],
        ])->label(false) ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
