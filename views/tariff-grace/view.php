<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TariffGrace */
?>
<div class="tariff-grace-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tariff_id',
            'type',
            'value',
        ],
    ]) ?>

</div>
