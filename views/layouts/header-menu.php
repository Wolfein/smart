<?php

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Главная', 'icon' => 'fa fa-bookmark', 'url' => ['/user1'],],
                    ['label' => 'Касса и банк', 'icon' => 'fa  fa-credit-card', 'url' => '#', 'options' => ['class' => 'has-sub'], 'items' => [
                        ['label' => 'Добавить кассовую/банковскую', 'url' => ['/user1'],],
                        ['label' => 'Кассовые и банковские операции', 'url' => ['/user1'],],
                        ['label' => 'Расчетные счета', 'url' => ['/user1'],],
                        ['label' => 'Закрытые смены', 'url' => ['/user1'],],
                    ]],
                    ['label' => 'Каталог', 'icon' => 'fa  fa-briefcase', 'url' => '/catalog/', 'options' => ['class' => 'has-sub'], 'items' => [
                        ['label' => 'Скупка', 'url' => ['/catalog/buying'],],
                        ['label' => 'Залог', 'url' => ['/catalog/deposit'],],
                        ['label' => 'Ремонт', 'url' => ['/catalog/repairs'],],
                        ['label' => 'Аксессуары', 'url' => ['/catalog/accesories'],],
                    ]],
                    ['label' => 'Журнал', 'icon' => 'fa   fa-list-alt', 'url' => ['/user1'],],
                    ['label' => 'Клиенты', 'icon' => 'fa  fa-user', 'url' => ['/client'],],
                    ['label' => 'База знаний', 'icon' => 'fa  fa-info', 'url' => ['/books'],],
                    ['label' => 'Справочники', 'icon' => 'fa  fa-book', 'url' => '#',  'visible' => Yii::$app->user->identity->isSuperAdmin(), 'options' => ['class' => 'has-sub'], 'items' => [
                        ['label' => 'Аналитика', 'url' => ['/comment'],],
                        ['label' => 'Пустые бланки документов', 'url' => ['/comment'],],
                        ['label' => 'Использованные ЗБ', 'url' => ['/comment'],],
                        ['label' => 'Оприходованные бланки ЗБ', 'url' => ['/comment'],],
                        ['label' => 'Списанные бланки ЗБ', 'url' => ['/comment'],],
                        ['label' => 'Перемещение товаров', 'url' => ['/comment'],],
                        ['label' => 'Отчёты', 'url' => ['/comment'],],
                        ['label' => 'Ценники', 'url' => ['/comment'],],
                        ['label' => 'Уценка товара', 'url' => ['/comment'],],
                    ]],
                    ['label' => 'Управление', 'icon' => 'fa  fa-cog', 'url' => '#',  'visible' => Yii::$app->user->identity->isSuperAdmin(), 'options' => ['class' => 'has-sub'], 'items' => [
                        ['label' => 'Пользователи', 'url' => ['/user'],],
                        ['label' => 'Роли', 'url' => ['/role'],],
                        ['label' => 'Города', 'url' => ['/city'],],
                        ['label' => 'Филиалы', 'url' => ['/wokplaces'],],
                        ['label' => 'Места хранения', 'url' => ['/storage'],],
                        ['label' => 'Тарифф', 'url' => ['/tariff'],],
                        ['label' => 'Категории', 'url' => ['/categories'],],
                        ['label' => 'Аксессуары', 'url' => ['/accessories'],],
                        ['label' => 'Мастера по ремонту', 'url' => ['/repairs'],],
                        ['label' => 'Карты лояльности', 'url' => ['/loyalty-cards'],],
                        ['label' => 'Рейтинг', 'url' => ['/clients-rating'],],
                        ['label' => 'Реквизиты', 'url' => ['/entities'],],
                        ['label' => 'Номерация', 'url' => ['/pawn-print'],],
                        ['label' => 'Типы документов', 'url' => ['/documents-type'],],
                        ['label' => 'Кассовые операции', 'url' => ['/cash-operations-basis'],],
                        ['label' => 'Рекламные каналы', 'url' => ['/advertising-channels'],],
                        ['label' => 'Шаблоны', 'url' => ['/templates'],],
                    ]],
                    ['label' => 'Добавить операцию', 'icon' => 'fa fa-plus-square','options' => ['class' =>'btn-warning', 'style' => 'background-color: #9c5013;    margin-left: 55px;'], 'url' => ['/operation/new'],],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
