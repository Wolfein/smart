<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
?>
<div class="client-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'surname',
            'name',
            'middle',
            'birthday',
            'city',
            'street',
            'home',
            'room',
            'actual_address',
            'place_birth',
            'phone',
            'channels_id',
            'cards_id',
            'email:email',
            'nationality',
            'inn',
            'snils',
            'pattern:ntext',
            'info:ntext',
            'create_at',
            'user_id',
        ],
    ]) ?>

</div>
