<?php

use app\models\ClientDoc;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */




if($model->isNewRecord == false){
    $model->doc = ClientDoc::find()->where(['client_id' => $model->id])->asArray()->all();

    if(count($model->doc) > 0){
        $lastId = $model->doc[count($model->doc)-1]['id'];
    }
}
?>

<div class="client-form">
    <div class="row">
            <?php $form = ActiveForm::begin(); ?>

           <div class="col-md-3">
                <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'middle')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'birthday')->textInput(['type'=>'date']) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'home')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'room')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'actual_address')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'place_birth')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'channels_id')->dropDownList($model->getListChanel()) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'cards_id')->dropDownList($model->getListCard()) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'nationality')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'inn')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'snils')->textInput(['maxlength' => true]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'pattern')->textarea(['rows' => 8]) ?>
            </div>

           <div class="col-md-3">
                <?= $form->field($model, 'info')->textarea(['rows' => 8]) ?>
            </div>
            <div class="col-md-3">
                <?php if($model->isNewRecord): ?>
                    <?= \kato\DropZone::widget([
                        'id'        => 'dzImage', // <-- уникальные id
                        'uploadUrl' => \yii\helpers\Url::toRoute([ 'client/upload-file' ]),
                        'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                        'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                        'clientEvents' => [
                            'success' => "function(file, response){ 
                                response = JSON.parse(response);
                                   
                                var id = response.record.id;
                                
                                var value = $('#task-files').val();
                                $('#task-files').val(value+','+id);
                             }",
                            'complete' => "function(file){ console.log(file); }",
                        ],
                    ]);?>
                    <div class="hidden">
                        <?= $form->field($model, 'files')->hiddenInput() ?>
                    </div>
                <?php else: ?>
                    <?= \kato\DropZone::widget([
                        'id'        => 'dzImage', // <-- уникальные id
                        'uploadUrl' => \yii\helpers\Url::toRoute([ 'client/upload-file', 'client_id' => $model->id]),
                        'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                        'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                    ]);?>
                <?php endif; ?>
            </div>

        <div class="col-md-12">
            <h4>Документы</h4>
            <?= $form->field($model, 'doc')->widget(MultipleInput::className(), [
                'id' => 'my_id',
                'min' => 0,
                'columns' => [
                    [
                        'name' => 'id',
                        'options' => [
                            'type' => 'hidden',
                        ]
                    ],
                    [
                        'name' => 'type',
                        'type'  => 'dropDownList',
                        'title' => 'Тип документа',
                        'items' => [
                            'Паспорт' => 'Паспорт',
                            'Права' => 'Права',
                            'Заграничный паспорт' => 'Заграничный паспорт',
                            'Вид на жительство' => 'Вид на жительство',
                            'Временное удостоверение личности' => 'Временное удостоверение личности',
                            'Военный билет' => 'Военный билет',
                            'Прочее' => 'Прочее',
                        ]
                    ],
                    [
                        'name' => 'number',
                        'title' => 'Серия, номер',
                    ],
                    [
                        'name' => 'who',
                        'title' => 'Кем выдан',
                    ],
                    [
                        'name' => 'date',
                        'type'  => \kartik\date\DatePicker::className(),
                        'title' => 'Дата выдачи',
                    ],
                    [
                        'name' => 'code',
                        'title' => 'Код подразделения',
                    ],
                ],
            ])->label(false) ?>
        </div>
    </div>


        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
