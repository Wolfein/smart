<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OperationDepositSubject */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operation-deposit-subject-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'deposit_id')->textInput() ?>

    <?= $form->field($model, 'cat_id')->dropDownList($model->getListCategory(),['prompt' => 'Выберите категорию']) ?>

    <?= $form->field($model, 'sub_cat_id')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ammount')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'storage_id')->dropDownList($model->getListStorage(),['prompt' => 'Выберите хранилище']) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textarea() ?>

    <?= $form->field($model, 'dt_status')->textInput(['type' => 'date']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
