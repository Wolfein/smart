<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OperationDepositSubject */

$this->title = 'Create Operation Deposit Subject';
$this->params['breadcrumbs'][] = ['label' => 'Operation Deposit Subjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operation-deposit-subject-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
