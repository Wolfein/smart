<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OperationDepositSubjectSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-6">
    <div class="operation-deposit-subject-search">

        <?php if(!$action){
            $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]);
        } else {
            $form = ActiveForm::begin([
                'action' => [$action],
                'method' => 'get',
            ]);
        }?>

    <!--    --><?//= $form->field($model, 'id') ?>

    <!--    --><?//= $form->field($model, 'deposit_id') ?>
        <div class="col-md-12">
            <?= $form->field($model, 'status')?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'cat_id')->dropDownList($model->getListCategory(),['prompt' => 'Все']) ?>
        </div>

    <!--    --><?//= $form->field($model, 'sub_cat_id')->dropDownList($model->getListCategory()) ?>

    <!--    --><?//= $form->field($model, 'name') ?>

    <!--    --><?//= $form->field($model, 'sn') ?>

    <!--    --><?//= $form->field($model, 'ammount') ?>

        <?php // echo $form->field($model, 'storage_id') ?>

        <?php // echo $form->field($model, 'description') ?>

        <?php // echo $form->field($model, 'comment') ?>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Очистить', ['class' => 'btn btn-outline-secondary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>