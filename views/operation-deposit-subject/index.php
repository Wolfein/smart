<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OperationDepositSubjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Operation Deposit Subjects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operation-deposit-subject-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Operation Deposit Subject', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'deposit_id',
            'cat_id',
            'sub_cat_id',
            'name',
            'sn',
            'ammount',
            //'storage_id',
            //'description:ntext',
            //'comment:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
