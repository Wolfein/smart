<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OperationDepositSubject */

$this->title = 'Update Operation Deposit Subject: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Operation Deposit Subjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="operation-deposit-subject-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
