<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DocumentsType */
?>
<div class="documents-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
