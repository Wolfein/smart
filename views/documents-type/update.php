<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DocumentsType */
?>
<div class="documents-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
