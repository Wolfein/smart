<?php
/**
 * Created by PhpStorm.
 * User: Wolfein
 * Date: 24.03.20
 * Time: 15:23
 */
use kartik\grid\GridView;
use yii\helpers\Html;
$this->title = 'Каталог "Залог"';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="panel panel-primary">
        <div class="panel-body">
            <?= $this->render('/operation-deposit-subject/_search',['model'=>$searchModel,'action' => '/catalog/deposit']) ?>
        </div>
    </div>
<?
$gridColumns = [
    [
        'attribute' => 'name',
        'label' => 'Наименование',
        'vAlign' => 'middle',
        'hAlign' => 'right',
        'pageSummary' => false,
    ],
    [
        'attribute' => 'sn',
        'label' => 'Серийный номер',
        'vAlign' => 'middle',
        'hAlign' => 'right',
        'pageSummary' => false,
    ],
    [
        'attribute' => 'ammount',
        'label' => 'Стоимость оценки',
        'vAlign' => 'middle',
        'hAlign' => 'right',
        'pageSummary' => false,
    ],
    [
        'attribute' => 'cat_id',
        'label' => 'Категория',
        'vAlign' => 'middle',
        'hAlign' => 'right',
        'pageSummary' => false,
        'value' => function($data){
                return $data->category->name;
            }
    ],
    [
        'attribute' => 'storage_id',
        'label' => 'Место хранения',
        'vAlign' => 'middle',
        'hAlign' => 'right',
        'pageSummary' => false,
        'value' => function($data){
                return $data->storage->name;
            }
    ],
    [
        'attribute' => 'description',
        'label' => 'Описание',
        'vAlign' => 'middle',
        'hAlign' => 'right',
        'pageSummary' => false,
    ],
    [
        'attribute' => 'comment',
        'label' => 'Комментарий',
        'vAlign' => 'middle',
        'hAlign' => 'right',
        'pageSummary' => false,
    ],
    [
        'attribute' => 'status',
        'label' => 'Статус',
        'vAlign' => 'middle',
        'hAlign' => 'right',
        'pageSummary' => false,
    ],
    [
        'attribute' => 'dt_status',
        'label' => 'Дата статуса',
        'vAlign' => 'middle',
        'hAlign' => 'right',
        'pageSummary' => false,
    ],
];
?>
<? echo GridView::widget([
    'id' => 'kv-grid-deposit',
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'containerOptions' => ['style' => 'overflow: auto'],
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => true,
    'toolbar' =>  [
        [
            'content' => '',
            'options' => ['class' => 'btn-group mr-2']
        ],
        '{export}',
        '{toggleData}',
    ],
    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => 'Список "Залог"',
    ],
    'persistResize' => false,
    'toggleDataOptions' => ['minCount' => 10],
//    'exportConfig' => $exportConfig,
    'itemLabelSingle' => '',
    'itemLabelPlural' => ''
]); ?>