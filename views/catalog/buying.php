<?php
/**
 * Created by PhpStorm.
 * User: Wolfein
 * Date: 24.03.20
 * Time: 15:23
 */
use kartik\grid\GridView;
$this->title = 'Каталог "Скупка"';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="panel panel-primary">
        <div class="panel-body">
            <?= $this->render('/operation/_search',['model'=>$searchModel,'action' => '/catalog/buying']) ?>
        </div>
    </div>

<?
    $gridColumns = [
        [
            'attribute' => 'type',
            'label' => 'Тип',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'client_id',
            'label' => 'Клиент',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'client_doc_id',
            'label' => 'Документ',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'categories_1_id',
            'label' => 'Категория',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'categories_2_id',
            'label' => 'Под категория',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'name',
            'label' => 'Наименование',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'number',
            'label' => 'Серийный номер',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'sum_pay',
            'label' => 'Сумма скупки',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'sum_out',
            'label' => 'Цена продажи, руб.',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'comis',
            'label' => 'Комиссионное вознаграждение, руб',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'markdows',
            'label' => 'Разрешить автоматическую уценку',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'r_s_id',
            'label' => 'Касса / Расчетный счет',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'info',
            'label' => 'Описание',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'channels_id',
            'label' => 'По какой рекламе пришли',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'comment',
            'label' => 'Комментарий',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'create_at',
            'label' => 'Создан',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'user_id',
            'label' => 'Кто создал',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],

    ];
?>
    <? echo GridView::widget([
    'id' => 'kv-grid-buying',
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'containerOptions' => ['style' => 'overflow: auto'],
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => true,
    'toolbar' =>  [
        [
            'content' => '',
            'options' => ['class' => 'btn-group mr-2']
        ],
        '{export}',
        '{toggleData}',
    ],
    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => 'Список "Скупка"',
    ],
    'persistResize' => false,
    'toggleDataOptions' => ['minCount' => 10],
//    'exportConfig' => $exportConfig,
    'itemLabelSingle' => '',
    'itemLabelPlural' => ''
]); ?>