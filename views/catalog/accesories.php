<?php
/**
 * Created by PhpStorm.
 * User: Wolfein
 * Date: 24.03.20
 * Time: 15:23
 */
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;

CrudAsset::register($this);

$this->title = 'Каталог "Аксессуары"';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-primary">
    <div class="panel-body">
        <?= $this->render('/operation-accesories/_search',['model'=>$searchModel,'action' => '/catalog/accesories']) ?>
    </div>
</div>
<?
    $gridColumns = [
        [
            'attribute' => 'cat_id',
            'label' => 'Категория',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
            'value' => function($data){
                    return $data->category->name;
                }
        ],
        [
            'attribute' => 'name',
            'label' => 'Наименование',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'count',
            'label' => 'Количество',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'ammount_purchase',
            'label' => 'Цена покупки (за 1 штуку), руб.',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'r_s_id',
            'label' => 'Касса / Расчетный счет',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'ammount_sell',
            'label' => 'Цена продажи (за 1 штуку), руб. ',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'datetime_',
            'label' => 'Дата/время операции',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],

    ];
?>
    <div class="panel panel-primary">
        <div class="panel-body">
            <?= Html::a('Купить аксессуары', ['/operation-accesories/create'],
                ['data-pjax'=>1, 'class'=>'btn btn-primary', 'title'=>'Купить аксессуары','role'=>'modal-remote']) ?>
        </div>
    </div>

    <? echo GridView::widget([
        'id' => 'kv-grid-accesories',
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true,
        'toolbar' =>  [
            [
                'content' => '',
                'options' => ['class' => 'btn-group mr-2']
            ],
            '{export}',
            '{toggleData}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => 'Список "Аксессуары"',
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10],
    //    'exportConfig' => $exportConfig,
        'itemLabelSingle' => '',
        'itemLabelPlural' => ''
    ]); ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
