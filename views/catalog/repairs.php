<?php
/**
 * Created by PhpStorm.
 * User: Wolfein
 * Date: 24.03.20
 * Time: 15:23
 */
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Каталог "Ремонт"';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="panel panel-primary">
        <div class="panel-body">
            <?= $this->render('/operation-repairs/_search',['model'=>$searchModel,'action' => '/catalog/repairs']) ?>
        </div>
    </div>
<?
    $gridColumns = [
        [
            'class' => 'kartik\grid\RadioColumn',
            'width' => '36px',
            'headerOptions' => ['class' => 'kartik-sheet-style'],
        ],
        [
            'attribute' => 'name',
            'label' => 'Наименовние',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'sn',
            'label' => 'Серийный номер',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'defect',
            'label' => 'Неисправность',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'turn_device',
            'label' => 'Состояние',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
            'value' => function($data){
                    if($data->turn_device == 0){
                        return 'не включается';
                    } else {
                        return 'включается';
                    }
                }
        ],
        [
            'attribute' => 'master_id',
            'label' => 'Мастер',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
            'value' => function($data){
                    return $data->master->name;
                }
        ],
        [
            'attribute' => 'datetime_',
            'label' => 'Дата/время операции',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'user_id',
            'label' => 'Кто создал',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
        [
            'attribute' => 'comment',
            'label' => 'Комментарий',
            'vAlign' => 'middle',
            'hAlign' => 'right',
            'pageSummary' => false,
        ],
    ];
?>
    <? echo GridView::widget([
    'id' => 'kv-grid-repairs',
    'dataProvider' => $dataProvider,
    'filterSelector' => '.second-filter',
    'columns' => $gridColumns,
    'containerOptions' => ['style' => 'overflow: auto'],
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => true,
    'toolbar' =>  [
        [
            'content' => '',
            'options' => ['class' => 'btn-group mr-2']
        ],
        '{export}',
        '{toggleData}',
    ],
    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => 'Список "Ремонт"',
    ],
    'persistResize' => false,
    'toggleDataOptions' => ['minCount' => 10],
//    'exportConfig' => $exportConfig,
    'itemLabelSingle' => '',
    'itemLabelPlural' => ''
]); ?>