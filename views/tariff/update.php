<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tariff */
?>
<div class="tariff-update">

    <?= $this->render('_formUpdate', [
        'model' => $model,
    ]) ?>

</div>
