<?php

use yii\bootstrap\Modal;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tariff */


\johnitvn\ajaxcrud\CrudAsset::register($this);

?>
<div class="candidate-view">

    <div class="row">

        <div class="col-md-4">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Тариф</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->render('@app/views/tariff/update', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Основной срок</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" ">
                    <?= $this->render('@app/views/tariff-interval/update', [
                        'model' => $tariffIntervalModel,
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Просрочка</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" >
                    <?= $this->render('@app/views/tariff-grace/update', [
                        'model' => $tariffGraceModel,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Дополнительные услуги</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" >
                    <?= $this->render('@app/views/tariff-extended/update', [
                        'model' => $tariffExtendedModel,
                    ]) ?>
                </div>
            </div>
        </div>


        <div class="col-md-4">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Описание</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <?= $this->render('@app/views/tariff-description/update', [
                        'model' => $tariffDescripttionModel,
                    ]) ?>
                </div>
            </div>
        </div>

    </div>
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


