<?php

use app\models\TariffDay;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\Tariff */
/* @var $form yii\widgets\ActiveForm */



if($model->isNewRecord == false){
    $model->bank = TariffDay::find()->where(['tariff_id' => $model->id])->asArray()->all();

    if(count($model->bank) > 0){
        $lastId = $model->bank[count($model->bank)-1]['id'];
    }
}


?>

<div class="tariff-form">

    <?php $form = ActiveForm::begin(['action' => '/tariff/update?id='.$model->id]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sum_min')->textInput() ?>

    <?= $form->field($model, 'type_roud')->dropDownList([
            'Без округления' => 'Без округления',
            'Округлять до 1 знака после запятой' => 'Округлять до 1 знака после запятой',
            'Округлять до целого значения' => 'Округлять до целого значения',
            'Округлять до значения кратного 10' => 'Округлять до значения кратного 10',
    ]) ?>

    <?= $form->field($model, 'count_day')->textInput() ?>

    <?= $form->field($model, 'actiont')->dropDownList(['1' => 'Да', '2' => 'Нет']) ?>

    <?= $form->field($model, 'wokplaces_id')->dropDownList($model->getListWokplaces()) ?>

    <h4>Интервал</h4>
    <?= $form->field($model, 'bank')->widget(MultipleInput::className(), [
        'id' => 'my_id3',
        'min' => 0,
        'columns' => [
            [
                'name' => 'id',
                'options' => [
                    'type' => 'hidden',
                ]
            ],
            [
                'name' => 'start',
                'title' => 'С',
            ],
            [
                'name' => 'end',
                'title' => 'По',
            ],
        ],
    ])->label(false) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
