<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tariff */

?>
<div class="tariff-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
