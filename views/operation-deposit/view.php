<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OperationDeposit */
?>
<div class="operation-deposit-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ammount',
            'r_s_id',
            'term_deposit',
            'tariff_id',
            'channels_id',
            'comment:ntext',
            'create_at',
            'user_id',
        ],
    ]) ?>

</div>
