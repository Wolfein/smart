<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;
use app\models\OperationDepositSubject;
/* @var $this yii\web\View */
/* @var $model app\models\OperationDeposit */
/* @var $form yii\widgets\ActiveForm */
?>

<? if($model->isNewRecord == false){
    $model->subjects = OperationDepositSubject::find()->where(['deposit_id' => $model->id])->asArray()->all();

    if(count($model->subjects) > 0){
        $lastId = $model->subjects[count($model->subjects)-1]['id'];
    }
}
?>

<div class="operation-deposit-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">
        <h4>Добавление имущества в залог</h4>
        <?= $form->field($model, 'subjects')->widget(MultipleInput::className(), [
            'id' => 'my_id',
            'min' => 0,
            'columns' => [
                [
                    'name' => 'id',
                    'options' => [
                        'type' => 'hidden',
                    ]
                ],
                [
                    'name' => 'deposit_id',
                    'options' => [
                        'type' => 'hidden',
                    ]
                ],
                [
                    'name' => 'cat_id',
                    'title' => 'Категория',
                    'type'  => 'dropDownList',
                    'defaultValue' => 1,
                    'items' => $model->getListCategory(),
                ],
                [
                    'name' => 'sub_cat_id',
                    'title' => 'Подкатегория',
                ],
                [
                    'name' => 'name',
                    'title' => 'Наименование',
                ],
                [
                    'name' => 'storage_id',
                    'title' => 'Место хранения',
                    'type'  => 'dropDownList',
                    'defaultValue' => 1,
                    'items' => $model->getListStorage(),
                ],
                [
                    'name' => 'description',
                    'title' => 'Описание',
                ],
                [
                    'name' => 'comment',
                    'title' => 'Комментарий',
                ],

            ],
        ])->label(false) ?>
    </div>

    <?= $form->field($model, 'ammount')->textInput() ?>

    <?= $form->field($model, 'r_s_id')->textInput() ?>

    <?= $form->field($model, 'term_deposit')->textInput() ?>

    <?= $form->field($model, 'tariff_id')->dropDownList($model->getListTarif(),['prompt' => 'Выберите тариф']) ?>

    <?= $form->field($model, 'channels_id')->textInput() ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'create_at')->textInput(['type' => 'date']) ?>

    <?= $form->field($model, 'user_id')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
