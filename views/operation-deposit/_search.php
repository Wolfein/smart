<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OperationDepositSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12">
<div class="operation-deposit-search">

    <?php if(!$action){
        $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]);
    } else {
        $form = ActiveForm::begin([
            'action' => [$action],
            'method' => 'get',
        ]);
    }?>
    <div class="col-md-3">
        <?= $form->field($model, 'status') ?>
    </div>

    <div class="col-md-3">
        <?= $form->field($model, 'create_at')->input('date') ?>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Очистить', ['class' => 'btn btn-outline-secondary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>