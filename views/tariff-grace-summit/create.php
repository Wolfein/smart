<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TariffGraceSummit */

?>
<div class="tariff-grace-summit-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
