<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TariffGraceSummit */
?>
<div class="tariff-grace-summit-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tariff_id',
            'day',
            'value',
        ],
    ]) ?>

</div>
