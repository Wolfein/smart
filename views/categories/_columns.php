<?php

use app\models\CategoriesSearch;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],


    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '50px',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detailUrl' => function ($model) {
//            return Url::toRoute(['view', 'id' => $model->id]);
        },
        'detail' => function ($model, $key, $index, $column) {
            $filesSearchModel = new CategoriesSearch();
            $filesDataProvider = $filesSearchModel->search([]);
            $filesDataProvider->query->andWhere(['categories_id' => $model->id]);

            return \Yii::$app->controller->renderPartial('_categories', [
                'model' => $model,
                'searchModel' => $filesSearchModel,
                'dataProvider' => $filesDataProvider,
                'categories_id' => $model->id,
            ]);
        },
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Показ',
        'content' => function($model){
            if ($model->eye == 1) {
                return Html::a('<i class="fa fa-eye text-success" style="font-size: 20px;"></i>', "/categories/eye?id={$model->id}&status=0", [
                    'role' => 'modal-remote', 'title' => 'Добавить в черный список',
                ]);
            } else {
                return  Html::a('<i class="fa fa fa-eye-slash text-danger" style="font-size: 20px;"></i>', "/categories/eye?id={$model->id}&status=1", [
                    'role'=>'modal-remote', 'title'=>'Отметить как не подходит',
                ]);
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tariff_id',
        'value' => 'tariff.name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'storage_id',
        'value' => 'storage.name',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   