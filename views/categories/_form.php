<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categories_id')->dropDownList($model->getListCategories(),['prompt' => 'Выберите категорию']) ?>

    <?= $form->field($model, 'tariff_id')->dropDownList($model->getListTariff(),['prompt' => 'Выберите тариф'])?>

    <?= $form->field($model, 'storage_id')->dropDownList($model->getListStorage(),['prompt' => 'Выберите склад']) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
