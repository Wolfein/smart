<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Templates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="templates-form">
    <div class="row">
    <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                                <?= $form->field($model, 'type')->dropDownList([
                        'Ломбард' => 'Ломбард',
                        'Комиссионный магазин' => 'Комиссионный магазин',
                        'Ремонт' => 'Ремонт',
                        'Аксессуары' => 'Аксессуары',
                        'Кассовые операции' => 'Кассовые операции',
                        'Шаблоны документов по заказу' => 'Шаблоны документов по заказу',
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>
        <br/>
        <br/>
        <div class="col-md-8">
            <div class="col-md-6">
                <?= $form->field($model, 'status')->checkbox() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'blank')->checkbox() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'dep')->checkbox() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'contin')->checkbox() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'contin_on')->checkbox() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'dep_rep')->checkbox() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'dobor')->checkbox() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'byuout_rem')->checkbox() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'byuout')->checkbox() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'out')->checkbox() ?>
            </div>
        </div>

            <div class="col-md-12">
                <?= $form->field($model, 'text')->widget(\mihaildev\ckeditor\CKEditor::class, []) ?>
            </div>


        </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    </div>
</div>
