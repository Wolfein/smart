<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Templates */
?>
<div class="templates-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'type',
            'description:ntext',
            'text:ntext',
            'status',
            'blank',
            'dep',
            'contin',
            'contin_on',
            'dep_rep',
            'dobor',
            'byuout_rem',
            'byuout',
            'out',
        ],
    ]) ?>

</div>
