<?php

use app\models\CategoriesSearch;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Are you sure?',
            'data-confirm-message'=>'Are you sure want to delete this item'],
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Показ',
        'content' => function($model){
            if ($model->eye == 1) {
                return Html::a('<i class="fa fa-eye text-success" style="font-size: 20px;"></i>', "/accessories/eye?id={$model->id}&status=0", [
                    'role' => 'modal-remote', 'title' => 'Добавить в черный список',
                ]);
            } else {
                return  Html::a('<i class="fa fa-eye-slash text-danger" style="font-size: 20px;"></i>', "/accessories/eye?id={$model->id}&status=1", [
                    'role'=>'modal-remote', 'title'=>'Отметить как не подходит',
                ]);
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],

];