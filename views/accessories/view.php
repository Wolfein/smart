<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Accessories */
?>
<div class="accessories-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'accessories_id',
        ],
    ]) ?>

</div>
