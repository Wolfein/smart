<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CashOperationsBasis */
?>
<div class="cash-operations-basis-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
