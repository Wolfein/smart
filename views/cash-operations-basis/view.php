<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CashOperationsBasis */
?>
<div class="cash-operations-basis-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'type',
            'cas',
        ],
    ]) ?>

</div>
