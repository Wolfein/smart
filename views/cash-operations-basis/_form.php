<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CashOperationsBasis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cash-operations-basis-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([
            'Приход' => 'Приход',
            'Приход / записать в доход' => 'Приход / записать в доход',
            'Расход' => 'Расход',
            'Расход / записать в затраты' => 'Расход / записать в затраты',
    ]) ?>

    <?= $form->field($model, 'cas')->dropDownList([
        'Касса' => 'Касса',
        'Расчетный счет' => 'Расчетный счет',
    ]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
