<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsRating */
?>
<div class="clients-rating-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'deposit',
            'pay',
            'pay_real',
            'self',
            'outpay',
            'reserve',
        ],
    ]) ?>

</div>
