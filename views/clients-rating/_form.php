<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsRating */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="panel panel-inverse news-index">

    <div class="panel-heading">
        <h4 class="panel-title">Рейтинг</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'deposit')->textInput() ?>

                <?= $form->field($model, 'pay')->textInput() ?>

                <?= $form->field($model, 'pay_real')->textInput() ?>

                <?= $form->field($model, 'self')->textInput() ?>

                <?= $form->field($model, 'outpay')->textInput() ?>

                <?= $form->field($model, 'reserve')->textInput() ?>

                <?php if (!Yii::$app->request->isAjax){ ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
