<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsRating */
?>
<div class="clients-rating-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
