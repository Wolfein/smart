<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entities */
?>
<div class="entities-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
