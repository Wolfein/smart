<?php

use app\models\EntitiesBank;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\Entities */
/* @var $form yii\widgets\ActiveForm */


if($model->isNewRecord == false){
    $model->bank = EntitiesBank::find()->where(['entities_id' => $model->id])->asArray()->all();

    if(count($model->bank) > 0){
        $lastId = $model->bank[count($model->bank)-1]['id'];
    }
}
?>

<div class="entities-form">
    <div class="row">
        
        <?php $form = ActiveForm::begin(); ?>
    
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'director')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'buh')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'inn')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'kpp')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'ogrn')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'okpo')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'oktmo')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-12">
            <h4>Банковские реквизиты</h4>
            <?= $form->field($model, 'bank')->widget(MultipleInput::className(), [
                'id' => 'my_id',
                'min' => 0,
                'columns' => [
                    [
                        'name' => 'id',
                        'options' => [
                            'type' => 'hidden',
                        ]
                    ],
                    [
                        'name' => 'r_s',
                        'title' => 'p/c',
                    ],
                    [
                        'name' => 'bank',
                        'title' => 'Банк',
                    ],
                    [
                        'name' => 'k_s',
                        'title' => 'к/с',
                    ],
                    [
                        'name' => 'bik',
                        'title' => 'БИК',
                    ],
                    [
                        'name' => 'eye',
                        'type'  => 'dropDownList',
                        'title' => 'Показ',
                        'items' => [
                            '1' => 'Да',
                            '0' => 'Нет',
                        ]
                    ],
                ],
            ])->label(false) ?>
        </div>
    </div>


      
        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>
    
        <?php ActiveForm::end(); ?>
</div>
