<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Entities */
?>
<div class="entities-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'address',
            'phone',
            'director',
            'buh',
            'inn',
            'kpp',
            'ogrn',
            'okpo',
            'oktmo',
        ],
    ]) ?>

</div>
