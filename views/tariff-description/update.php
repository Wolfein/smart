<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TariffDescription */
?>
<div class="tariff-description-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
