<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TariffDescription */

?>
<div class="tariff-description-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
