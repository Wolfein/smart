<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TariffDescription */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tariff-description-form">


    <?php $form = ActiveForm::begin(['action' => '/tariff-description/update?id='.$model->id]); ?>


    <?= $form->field($model, 'loan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'old')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
