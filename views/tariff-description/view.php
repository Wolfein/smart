<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TariffDescription */
?>
<div class="tariff-description-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tariff_id',
            'loan',
            'old',
        ],
    ]) ?>

</div>
