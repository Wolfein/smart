<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OperationSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12">
    <div class="operation-search">
        <?php if(!$action){
            $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]);
        } else {
            $form = ActiveForm::begin([
                'action' => [$action],
                'method' => 'get',
            ]);
        }?>
        <div class="col-md-3">
            <?= $form->field($model, 'type') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'client_id')->dropDownList($model->getListClient(),['prompt' => 'Все']) ?>
        </div>

<!--            --><?//= $form->field($model, 'client_doc_id') ?>

        <div class="col-md-3">
            <?= $form->field($model, 'categories_1_id')->dropDownList($model->getListCategory(),['prompt' => 'Все']) ?>
        </div>

        <?php // echo $form->field($model, 'categories_2_id') ?>

        <?php // echo $form->field($model, 'name') ?>

        <?php // echo $form->field($model, 'number') ?>

        <?php // echo $form->field($model, 'sum_pay') ?>

        <?php // echo $form->field($model, 'sum_out') ?>

        <?php // echo $form->field($model, 'comis') ?>

        <?php // echo $form->field($model, 'markdows') ?>

        <?php // echo $form->field($model, 'r_s_id') ?>

        <?php // echo $form->field($model, 'info') ?>

        <?php // echo $form->field($model, 'channels_id') ?>

        <?php // echo $form->field($model, 'comment') ?>

        <?php // echo $form->field($model, 'create_at') ?>

        <?php // echo $form->field($model, 'user_id') ?>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Очистить', ['class' => 'btn btn-outline-secondary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

