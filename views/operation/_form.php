<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Operation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operation-form">
    <div class="row">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-6">
        <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'client_id')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'client_doc_id')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'categories_1_id')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'categories_2_id')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'sum_pay')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'sum_out')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'comis')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'markdows')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'r_s_id')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'info')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'channels_id')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
    </div>


  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
