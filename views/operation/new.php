<?php
/**
 * Created by PhpStorm.
 * User: Wolfein
 * Date: 24.03.20
 * Time: 12:15
 */

use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

CrudAsset::register($this);

$this->title = 'Добавление операции';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Добавление операции</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <?= Html::dropDownList('s_id', null,$ClientList,[
                        'class' => 'form-control',
                        'prompt' => 'Клиент',
                        'id' => 'client',
                        'onChange'=>'$(document).ready(function() {
                                if ( document.getElementById("client").options.selectedIndex == 0) {
                                    $("#accesories").show();
                                    $("#deposit").hide();
                                    $("#operation").hide();
                                    $("#repairs").hide();
                                } else {
                                    $("#accesories").show();
                                    $("#deposit").show();
                                    $("#operation").show();
                                    $("#repairs").show();
                                }
                            });'
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <?= Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/client/create'],
                        ['data-pjax'=>1, 'class'=>'btn btn-primary', 'title'=>'Добавить клиента','role'=>'modal-remote']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="padding: 15px">
                    <?= Html::a('Приём на ремонт', ['/operation-repairs/create'],
                        ['id'=>'repairs','data-pjax'=>1, 'class'=>'btn btn-primary', 'title'=>'Добавить клиента','role'=>'modal-remote','style'=>['display'=>'none']]) ?>
                    <?= Html::a('Покупка аксессуаров', ['/operation-accesories/create'],
                        ['id'=>'accesories','data-pjax'=>1, 'class'=>'btn btn-primary', 'title'=>'Добавить клиента','role'=>'modal-remote']) ?>
                    <?= Html::a('Залог', ['/operation-deposit/create'],
                        ['id'=>'deposit','data-pjax'=>1, 'class'=>'btn btn-primary', 'title'=>'Добавить клиента','role'=>'modal-remote','style'=>['display'=>'none']]) ?>
                    <?= Html::a('Операции', ['/operation/create'],
                        ['id'=>'operation','data-pjax'=>1, 'class'=>'btn btn-primary', 'title'=>'Добавить клиента','role'=>'modal-remote','style'=>['display'=>'none']]) ?>
                </div>
            </div>


        </div>
    </div>

</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => ['class' => 'fade modal-slg'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
