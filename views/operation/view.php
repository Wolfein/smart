<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Operation */
?>
<div class="operation-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'client_id',
            'client_doc_id',
            'categories_1_id',
            'categories_2_id',
            'name',
            'number',
            'sum_pay',
            'sum_out',
            'comis',
            'markdows',
            'r_s_id',
            'info:ntext',
            'channels_id',
            'comment:ntext',
            'create_at',
            'user_id',
        ],
    ]) ?>

</div>
