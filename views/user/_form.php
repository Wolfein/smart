<?php

use app\models\UsersWokplaces;
use app\models\Wokplaces;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false) {
    $userWorkk = ArrayHelper::getColumn(UsersWokplaces::find()->where(['user_id' => $model->id])->all(), 'wokplaces_id');
    $model->wokplace = ArrayHelper::getColumn(Wokplaces::find()->where(['id' => $userWorkk])->all(), 'id');
}
?>


<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'data-toggle' => 'tooltip'])?>
    <?= $form->field($model, 'role')->dropDownList($model->getListRole()) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'action_info')->textarea(['maxlength' => true]) ?>
    <?= $form->field($model, 'status')->dropDownList(['0' => 'Отключен','1' => 'Включен']) ?>
    <?= $form->field($model, 'comment')->textarea(['maxlength' => true]) ?>
    <?= $form->field($model, 'wokplace')->widget(\kartik\select2\Select2::className(), [
        'data' => ArrayHelper::map(Wokplaces::find()->all(), 'id', 'name'),
        'options' => [
            'multiple' => true,
        ],
        'pluginOptions' => [
            'tags' => false,
            'tokenSeparators' => [','],
        ],
    ]) ?>
    <?= $form->field($model, 'file')->fileInput() ?>



	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
