<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TariffExtended */
?>
<div class="tariff-extended-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
