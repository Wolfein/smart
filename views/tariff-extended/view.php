<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TariffExtended */
?>
<div class="tariff-extended-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tariff_id',
            'day',
            'value',
        ],
    ]) ?>

</div>
