<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Role */
?>
<div class="role-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'comment',
            'l_1',
            'l_2',
            'l_3',
            'l_4',
            'l_5',
            'l_6',
            'c_1',
            'c_2',
            'c_3',
            'c_4',
            'c_5',
            'c_6',
            'r_1',
            'r_2',
            'a_1',
            'a_2',
            'a_3',
            'o_1',
            'o_2',
            'o_3',
            'o_4',
            'o_5',
            'o_6',
            'o_7',
            'o_8',
            'o_9',
            'o_10',
            's_1',
            's_2',
            's_3',
            's_4',
            's_5',
            's_6',
            's_7',
            's_8',
            's_9',
            's_10',
            's_11',
            's_12',
            's_13',
            's_14',
            's_15',
            's_16',
            's_17',
            's_18',
            's_19',
            't_1',
            'k_1',
            'k_2',
            'k_3',
            'k_4',
            'k_5',
            'k_6',
            'k_7',
            'k_8',
            'k_9',
            'an_1',
            'an_2',
            'rep_1',
            'rep_2',
            'rep_3',
            'rep_4',
            'rep_5',
            'rep_6',
            'rep_7',
            'cl_1',
            'cl_2',
            'cl_3',
        ],
    ]) ?>

</div>
