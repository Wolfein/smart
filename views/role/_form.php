<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Role */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="role-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
        <h4>Ломбард</h4>
        <hr class="hor-line">
    <div class="row" style="font-size: 10px">
        <div class="col-md-3">
                        <?= $form->field($model, 'l_1')->checkbox() ?>
        </div>
        <div class="col-md-3">
                        <?= $form->field($model, 'l_1_1')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'l_1_2')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'l_1_3')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'l_1_4')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'l_1_5')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'l_1_6')->checkbox() ?>
        </div>
        <div class="col-md-3">
                        <?= $form->field($model, 'l_2')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'l_3')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'l_4')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'l_5')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'l_6')->checkbox() ?>
        </div>
    </div>
        <h4>Комиссионный магазин</h4>
        <hr class="hor-line">
    <div class="row" style="font-size: 10px">
        <div class="col-md-3">
                        <?= $form->field($model, 'c_1')->checkbox() ?>
        </div>
        <div class="col-md-3">
                        <?= $form->field($model, 'c_1_1')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'c_1_2')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'c_1_3')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'c_1_4')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'c_1_5')->checkbox() ?>
        </div>
        <div class="col-md-3">
                        <?= $form->field($model, 'c_1_6')->checkbox() ?>
        </div>
        <div class="col-md-3">
                        <?= $form->field($model, 'c_2')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'c_3')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'c_4')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'c_5')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'c_6')->checkbox() ?>
        </div>
    </div>
        <h4>Ремонт</h4>
        <hr class="hor-line">
    <div class="row">
        <div class="col-md-3">
                        <?= $form->field($model, 'r_1')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'r_2')->checkbox() ?>
        </div>
    </div>

        <h4>Аксессуары</h4>
        <hr class="hor-line">
    <div class="row">
        <div class="col-md-3">
                        <?= $form->field($model, 'a_1')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'a_2')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'a_3')->checkbox() ?>
        </div>
    </div>

        <h4>Общие возможности</h4>
        <hr class="hor-line">

    <div class="row">
        <div class="col-md-3">
                        <?= $form->field($model, 'o_1')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'o_2')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'o_3')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'o_4')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'o_5')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'o_6')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'o_7')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'o_8')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'o_9')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'o_10')->checkbox() ?>
        </div>
    </div>
        <h4>Управление → Настройки</h4>
        <hr class="hor-line">
    <div class="row">
        <div class="col-md-3">
                        <?= $form->field($model, 's_1')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_2')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_3')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_4')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_5')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_6')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_7')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_8')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_9')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_10')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_11')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_12')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_13')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_14')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_15')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_16')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_17')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_18')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 's_19')->checkbox() ?>
        </div>
    </div>
        <h4>Касса и банк</h4>
        <hr class="hor-line">
    <div class="row">
        <div class="col-md-3">
                        <?= $form->field($model, 'k_1')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'k_2')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'k_3')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'k_4')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'k_5')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'k_6')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'k_7')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'k_8')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'k_9')->checkbox() ?>
        </div>
    </div>

        <h4>Аналитика и отчеты</h4>
        <hr class="hor-line">
    <div class="row">
        <div class="col-md-3">
                        <?= $form->field($model, 'an_1')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'an_2')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'rep_1')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'rep_2')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'rep_3')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'rep_4')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'rep_5')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'rep_6')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'rep_7')->checkbox() ?>
        </div>
    </div>
        <h4>Прочее</h4>
        <hr class="hor-line">
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 't_1')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'cl_1')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'cl_2')->checkbox() ?>
        </div>

        <div class="col-md-3">
                        <?= $form->field($model, 'cl_3')->checkbox() ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
