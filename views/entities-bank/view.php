<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EntitiesBank */
?>
<div class="entities-bank-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'entities_id',
            'r_s',
            'bank',
            'k_s',
            'bik',
        ],
    ]) ?>

</div>
