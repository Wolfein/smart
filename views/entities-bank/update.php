<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EntitiesBank */
?>
<div class="entities-bank-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
