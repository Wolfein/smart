<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OperationRepairs */
?>
<div class="operation-repairs-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'sn',
            'defect',
            'turn_device',
            [
                'label' => 'Мастер',
                'attribute' => 'master.name',
            ],
            'datetime_',
            'user_id',
            'comment:ntext',
        ],
    ]) ?>

</div>
