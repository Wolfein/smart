<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OperationRepairsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-12">
    <div class="operation-repairs-search">

        <?php if(!$action){
            $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]);
        } else {
            $form = ActiveForm::begin([
                'action' => [$action],
                'method' => 'get',
            ]);
        }?>

    <!--    --><?//= $form->field($model, 'id') ?>
<!--        <div class="col-md-3">-->
<!--            --><?//= $form->field($model, 'name') ?>
<!--        </div>-->
    <!--            --><?//= $form->field($model, 'sn') ?>
        <div class="col-md-3">

            <?= $form->field($model, 'status') ?>
        </div>
    <!--    --><?//= $form->field($model, 'defect') ?>

    <!--    --><?//= $form->field($model, 'turn_device') ?>
        <div class="col-md-3">
            <?=   $form->field($model, 'master_id')->dropDownList($model->getListMaster(),['prompt' => 'Все']) ?>
        </div>
        <?php // echo $form->field($model, 'datetime_') ?>

        <?php // echo $form->field($model, 'user_id') ?>

        <?php // echo $form->field($model, 'comment') ?>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Очистить', ['class' => 'btn btn-outline-secondary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
