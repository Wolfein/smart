<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OperationRepairs */

?>
<div class="operation-repairs-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
