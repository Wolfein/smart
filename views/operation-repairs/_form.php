<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OperationRepairs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operation-repairs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'defect')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'turn_device')->radioList(['1' => 'Да', '0' => 'Нет', ]);?>

    <?= $form->field($model, 'master_id')->dropDownList($model->getListMaster()) ?>

    <?= $form->field($model, 'datetime_')->textInput(['type' => 'date']) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
