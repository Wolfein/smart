<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OperationAccesories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operation-accesories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cat_id')->dropDownList($model->getListAccessories(),['prompt' => 'Выберите категорию']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'count')->textInput() ?>

    <?= $form->field($model, 'ammount_purchase')->textInput() ?>

    <?= $form->field($model, 'r_s_id')->textInput() ?>

    <?= $form->field($model, 'ammount_sell')->textInput() ?>

    <?= $form->field($model, 'datetime_')->textInput(['type' => 'date']) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
