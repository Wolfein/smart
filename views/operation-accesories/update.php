<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OperationAccesories */
?>
<div class="operation-accesories-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
