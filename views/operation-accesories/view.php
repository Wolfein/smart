<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OperationAccesories */
?>
<div class="operation-accesories-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cat_id',
            'name',
            'count',
            'ammount_purchase',
            'r_s_id',
            'ammount_sell',
            'datetime_',
        ],
    ]) ?>

</div>
