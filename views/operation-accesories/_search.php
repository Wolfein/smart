<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OperationAccesoriesSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-6">
    <div class="operation-accesories-search">
        <?php if(!$action){
            $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]);
        } else {
            $form = ActiveForm::begin([
                'action' => [$action],
                'method' => 'get',
            ]);
        }?>

<!--        --><?//= $form->field($model, 'id') ?>
        <div class="col-md-6">
            <?= $form->field($model, 'cat_id')->dropDownList($model->getListCategory(),['prompt' => 'Все']) ?>
        </div>
<!--        --><?//= $form->field($model, 'name') ?>

<!--        --><?//= $form->field($model, 'count') ?>

<!--        --><?//= $form->field($model, 'ammount_purchase') ?>

        <?php // echo $form->field($model, 'r_s_id') ?>

        <?php // echo $form->field($model, 'ammount_sell') ?>

        <?php // echo $form->field($model, 'datetime_') ?>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Очистить', ['class' => 'btn btn-outline-secondary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
