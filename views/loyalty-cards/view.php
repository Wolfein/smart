<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LoyaltyCards */
?>
<div class="loyalty-cards-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'pay',
            'pay_real',
        ],
    ]) ?>

</div>
