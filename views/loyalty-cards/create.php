<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LoyaltyCards */

?>
<div class="loyalty-cards-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
