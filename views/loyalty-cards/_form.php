<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LoyaltyCards */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loyalty-cards-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'buyout')->textInput() ?>

    <?= $form->field($model, 'extension')->textInput() ?>

    <?= $form->field($model, 'selected')->textInput() ?>

    <?= $form->field($model, 'part')->textInput() ?>

    <?= $form->field($model, 'pay_acs')->textInput() ?>

    <?= $form->field($model, 'real')->textInput() ?>

    <?= $form->field($model, 'cash')->textInput() ?>

    <?= $form->field($model, 'pay')->textInput() ?>

    <?= $form->field($model, 'pay_real')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
