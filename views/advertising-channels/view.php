<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AdvertisingChannels */
?>
<div class="advertising-channels-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'add_cl',
            'dep',
            'pay',
            'pay_self',
            'self',
            'self_acs',
            'add_rem',
        ],
    ]) ?>

</div>
