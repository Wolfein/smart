<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AdvertisingChannels */

?>
<div class="advertising-channels-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
