<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PawnPrint */
?>
<div class="pawn-print-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
