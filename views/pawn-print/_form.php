<?php

use app\models\Wokplaces;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\PawnPrint */
/* @var $form yii\widgets\ActiveForm */



if($model->isNewRecord == false){
        $wok = Wokplaces::find()->all();
        foreach ($wok as $item) {
            $model->ser = $item->ser;
            $model->number = $item->number;
            break;
        }
        $model->bank = Wokplaces::find()->asArray()->all();
        if(count($model->bank) > 0){
            $lastId = $model->bank[count($model->bank)-1]['id'];
        }
}
?>

<div class="panel panel-inverse news-index">
    <div class="panel-heading">
        <h4 class="panel-title">Настройка печати и нумерации залоговых билетов</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'entities_id')->dropDownList($model->getListEntities()) ?>

                <?= $form->field($model, 'type_print')->dropDownList([
                        'Печать на типографском бланке' => 'Печать на типографском бланке',
                        'Бланк СмартЛомбард' => 'Бланк СмартЛомбард',

                    ]) ?>

                <?= $form->field($model, 'type_number')->dropDownList([
                    'Общая' => 'Общая серия и нумерация для всех филиалов',
                    'Отдельная' => 'Отдельная серия и нумерация для каждого филиала',

                ],['id'=>'type', 'onChange'=>'$(document).ready(function() {
                        if ( document.getElementById("type").value == "Общая") {
                            document.getElementById("list").hidden=true; 
                            document.getElementById("one").hidden=false; 
                        } else {
                            document.getElementById("list").hidden=false; 
                            document.getElementById("one").hidden=true; 
                        }
                    });']) ?>
                <div id="list" <?php if ($model->type_number == 'Общая') echo 'hidden'?>>
                    <h4>Филиалы</h4>
                    <?= $form->field($model, 'bank')->widget(MultipleInput::className(), [
                        'id' => 'my_id',
                        'min' => count($model->bank),
                        'max' => count($model->bank),
                        'columns' => [
                            [
                                'name' => 'id',
                                'options' => [
                                    'type' => 'hidden',
                                ]
                            ],
                            [
                                'name' => 'ser',
                                'type'  => 'dropDownList',
                                'title' => 'Серия',
                                'items' => [$model->getListSer()]
                            ],
                            [
                                'name' => 'number',
                                'title' => 'Номер',
                            ],
                        ],
                    ])->label(false) ?>
                </div>
                <div id="one" <?php if ($model->type_number != 'Общая') echo 'hidden'?>>
                    <?= $form->field($model, 'ser')->textInput() ?>
                    <?= $form->field($model, 'number')->textInput() ?>

                </div>
                <?php if (!Yii::$app->request->isAjax){ ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    
</div>
