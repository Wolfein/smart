<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PawnPrint */
?>
<div class="pawn-print-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'entities_id',
            'type_print',
            'type_number',
        ],
    ]) ?>

</div>
