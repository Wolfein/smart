<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Repairs */
?>
<div class="repairs-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'phone',
            'doc',
            'doc_info',
            'doc_who',
            'doc_date',
        ],
    ]) ?>

</div>
