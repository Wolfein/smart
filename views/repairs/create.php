<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Repairs */

?>
<div class="repairs-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
