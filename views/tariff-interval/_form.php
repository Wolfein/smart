<?php

use app\models\TariffIntervalSummit;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\TariffInterval */
/* @var $form yii\widgets\ActiveForm */


if($model->isNewRecord == false){
    $model->bank = TariffIntervalSummit::find()->where(['tariff_id' => $model->id])->asArray()->all();

    if(count($model->bank) > 0){
        $lastId = $model->bank[count($model->bank)-1]['id'];
    }
}
?>

<div class="tariff-interval-form">

    <?php $form = ActiveForm::begin(['action' => '/tariff-interval/update?id='.$model->id]); ?>


    <?= $form->field($model, 'limit_max')->textInput() ?>

    <?= $form->field($model, 'limit_min')->textInput() ?>

    <?= $form->field($model, 'type_roud')->dropDownList([
            'Без округления' => 'Без округления',
            'До срока займа' => 'До срока займа',
            'Свое значение' => 'Свое значение',
    ]) ?>

    <?= $form->field($model, 'days_calc')->dropDownList([
        'Обычный расчет' => 'Обычный расчет',
        'Прибавлять дополнительный день' => 'Прибавлять дополнительный день',
    ]) ?>

    <h4>Процентные ставки по займу, % в день</h4>
    <?= $form->field($model, 'bank')->widget(MultipleInput::className(), [
        'id' => 'my_id',
        'min' => 0,
        'columns' => [
            [
                'name' => 'id',
                'options' => [
                    'type' => 'hidden',
                ]
            ],
            [
                'name' => 'day',
                'title' => 'Интервал',
            ],
            [
                'name' => 'value',
                'title' => 'Значение',
            ],
        ],
    ])->label(false) ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
