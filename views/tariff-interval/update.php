<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TariffInterval */
?>
<div class="tariff-interval-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
