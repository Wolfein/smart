<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TariffInterval */
?>
<div class="tariff-interval-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tariff_id',
            'limit_max',
            'limit_min',
            'type_roud',
            'days_calc',
        ],
    ]) ?>

</div>
