<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Storage */
?>
<div class="storage-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
