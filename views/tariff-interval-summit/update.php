<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TariffIntervalSummit */
?>
<div class="tariff-interval-summit-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
