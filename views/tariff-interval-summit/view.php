<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TariffIntervalSummit */
?>
<div class="tariff-interval-summit-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tariff_id',
            'day',
            'sum',
            'value',
        ],
    ]) ?>

</div>
