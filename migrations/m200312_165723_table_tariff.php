<?php

use yii\db\Migration;

/**
 * Class m200312_165723_table_tariff
 */
class m200312_165723_table_tariff extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tariff', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'comment' => $this->string()->comment('Комментарий'),
            'sum_min' => $this->float()->comment('Минимальная сумма'),
            'sum_max' => $this->float()->comment('Максимальная сумма'),
            'type_roud' => $this->string()->comment('Округление суммы оплаты'),
            'count_day' => $this->integer()->comment('Количество первых бесплатных дней'),
            'actiont' => $this->boolean()->comment('Действие карты постоянного клиента'),
            'wokplaces_id' => $this->integer()->comment('Филиалы использования тарифа'),
        ]);

        $this->createTable('tariff_day', [
            'id' => $this->primaryKey(),
            'tariff_id' => $this->integer()->comment('Связь тарифом'),
            'start' => $this->integer()->comment('Максимальный срок займа, дней'),
            'end' => $this->integer()->comment('Минимальное количество дней'),
        ]);

        $this->createTable('tariff_interval', [
            'id' => $this->primaryKey(),
            'tariff_id' => $this->integer()->comment('Связь тарифом'),
            'limit_max' => $this->integer()->comment('Максимальный срок займа, дней'),
            'limit_min' => $this->integer()->comment('Минимальное количество дней'),
            'type_roud' => $this->string()->comment('Округление срока залога'),
            'days_calc' => $this->string()->comment('Расчет количества дней'),
        ]);

        $this->createTable('tariff_interval_summit', [
            'id' => $this->primaryKey(),
            'tariff_id' => $this->integer()->comment('Связь тарифом'),
            'day' => $this->integer()->comment('Интервал срок'),
            'sum' => $this->integer()->comment('Интервал сумм'),
            'value' => $this->float()->comment('Процент'),
        ]);

        $this->createTable('tariff_grace', [
            'id' => $this->primaryKey(),
            'tariff_id' => $this->integer()->comment('Связь тарифом'),
            'type' => $this->string()->comment('Тип расчета'),
            'value' => $this->integer()->comment('Процент'),
        ]);

        $this->createTable('tariff_grace_summit', [
            'id' => $this->primaryKey(),
            'tariff_id' => $this->integer()->comment('Связь тарифом'),
            'day' => $this->integer()->comment('Интервал дней'),
            'value' => $this->float()->comment('Процент'),
        ]);


        $this->createTable('tariff_extended', [
            'id' => $this->primaryKey(),
            'tariff_id' => $this->integer()->comment('Связь тарифом'),
            'day' => $this->float()->comment('Оплата хранения, руб. в день'),
            'value' => $this->float()->comment('Стоимость оценки, руб.'),
        ]);

        $this->createTable('tariff_description', [
            'id' => $this->primaryKey(),
            'tariff_id' => $this->integer()->comment('Связь тарифом'),
            'loan' => $this->string()->comment('Процентная ставка по займу'),
            'old' => $this->string()->comment('На прочие услуги'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tariff');
        $this->dropTable('tariff_interval');
        $this->dropTable('tariff_interval_summit');
        $this->dropTable('tariff_grace');
        $this->dropTable('tariff_grace_summit');
        $this->dropTable('tariff_extended');
        $this->dropTable('tariff_description');
    }
}
