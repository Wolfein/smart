<?php

use yii\db\Migration;

/**
 * Class m200313_054016_table_user_wokplaces
 */
class m200313_054016_table_user_wokplaces extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users_wokplaces', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Связь с пользователем'),
            'wokplaces_id' => $this->integer()->comment('Связь с филиалом'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users_wokplaces');
    }
}
