<?php

use yii\db\Migration;

/**
 * Class m200312_181303_table_documents_type
 */
class m200312_181303_table_documents_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('documents_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('documents_type');
    }
}
