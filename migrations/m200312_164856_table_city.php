<?php

use yii\db\Migration;

/**
 * Class m200312_164856_table_city
 */
class m200312_164856_table_city extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('city', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'region' => $this->string()->comment('Регион'),
            'money' => $this->string()->comment('Валюта'),
            'time_line' => $this->string()->comment('Часовой пояс'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('city');
    }
}
