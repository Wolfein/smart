<?php

use yii\db\Migration;

/**
 * Class m200312_180735_table_pawn_print
 */
class m200312_180735_table_pawn_print extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pawn_print', [
            'id' => $this->primaryKey(),
            'entities_id' => $this->integer()->comment('Связь реквитов'),
            'type_print' => $this->string()->comment('Вариант печати'),
            'type_number' => $this->string()->comment('Вариант нумерации'),
        ]);


        $this->insert('pawn_print', [
            'entities_id' => null,
            'type_print' => null,
            'type_number' => null,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pawn_print');
    }
}
