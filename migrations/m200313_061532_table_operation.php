<?php

use yii\db\Migration;

/**
 * Class m200313_061532_table_operation
 */
class m200313_061532_table_operation extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('operation', [
            'id' => $this->primaryKey(),
            'type' => $this->string()->comment('Тип'),
            'client_id' => $this->integer()->comment('Клиент'),
            'client_doc_id' => $this->integer()->comment('Документ'),
            'categories_1_id' => $this->integer()->comment('Категория'),
            'categories_2_id' => $this->integer()->comment('Под категория'),
            'name' => $this->string()->comment('Наименование'),
            'number' => $this->string()->comment('Серийный номер'),
            'sum_pay' => $this->float()->comment('Сумма скупки'),
            'sum_out' => $this->float()->comment('Цена продажи, руб.'),
            'comis' => $this->float()->comment('Комиссионное вознаграждение, руб'),
            'markdows' => $this->boolean()->comment('Разрешить автоматическую уценку'),
            'r_s_id' => $this->integer()->comment('Касса / Расчетный счет'),
            'info' => $this->text()->comment('Описание'),
            'channels_id' => $this->integer()->comment('По какой рекламе пришли'),
            'comment' => $this->text()->comment('Комментарий'),
            'create_at' => $this->string()->comment('Создан'),
            'user_id' => $this->string()->comment('Кто создал'),
        ]);


        $this->createTable('operation_photo', [
            'id' => $this->primaryKey(),
            'operation_id' => $this->integer()->comment('Cвязь'),
            'link' => $this->string()->comment('Тип документа'),
            'create_at' => $this->string()->comment('Создан'),
        ]);


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('operation');
        $this->dropTable('operation_photo');
    }
}
