<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170428_140722_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull()->comment('Логин'),
            'name' => $this->string()->comment('ФИО'),
            'photo' => $this->string()->comment('Фото'),
            'phone' => $this->string()->comment('Телефон'),
            'inn' => $this->string()->comment('ИНН'),
            'action_info' => $this->string()->comment('Действующего на основании'),
            'comment' => $this->string()->comment('Комментарий'),
            'role' => $this->integer()->comment('Роль'),
            'status' => $this->integer()->comment('Статус'),
            'password_hash' => $this->string()->notNull()->comment('Зашифрованный пароль'),
            'is_deletable' => $this->boolean()->notNull()->defaultValue(true)->comment('Можно удалить или нельзя'),
        ]);

        $this->insert('user', [
            'login' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'is_deletable' => false,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
