<?php

use yii\db\Migration;

/**
 * Class m200313_055010_table_client
 */
class m200313_055010_table_client extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('client', [
            'id' => $this->primaryKey(),
            'surname' => $this->string()->comment('Фамилия'),
            'name' => $this->string()->comment('Имя'),
            'middle' => $this->string()->comment('Отчество'),
            'birthday' => $this->date()->comment('Дата рождения'),
            'city' => $this->string()->comment('Нас. пункт'),
            'street' => $this->string()->comment('Улица'),
            'home' => $this->string()->comment('Дом'),
            'room' => $this->string()->comment('Квартира'),
            'actual_address' => $this->string()->comment('Фактический адрес проживания'),
            'place_birth' => $this->string()->comment('Место рождения'),
            'phone' => $this->string()->comment('Контактный телефон'),
            'channels_id' => $this->integer()->comment('Откуда о нас узнали'),
            'cards_id' => $this->integer()->comment('Карта'),
            'email' => $this->string()->comment('Электронная почта'),
            'nationality' => $this->string()->comment('Гражданство'),
            'inn' => $this->string()->comment('ИНН'),
            'snils' => $this->string()->comment('СНИЛС'),
            'pattern' => $this->text()->comment('Характеристика клиента'),
            'info' => $this->text()->comment('Предупреждение'),
            'create_at' => $this->string()->comment('Создан'),
            'user_id' => $this->string()->comment('Кто создал'),
        ]);

        $this->createTable('client_doc', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->comment('Cвязь с клиентом'),
            'type' => $this->string()->comment('Тип документа'),
            'number' => $this->string()->comment('Серия, номер'),
            'who' => $this->string()->comment('Кем выдан'),
            'date' => $this->string()->comment('Дата выдачи'),
            'code' => $this->string()->comment('Код подразделения'),
            'create_at' => $this->dateTime()->comment('Создан'),
        ]);

        $this->createTable('client_photo', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->comment('Cвязь с клиентом'),
            'link' => $this->string()->comment('Тип документа'),
            'create_at' => $this->string()->comment('Создан'),
        ]);


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('client');
        $this->dropTable('client_doc');
        $this->dropTable('client_photo');
    }
}
