<?php

use yii\db\Migration;

/**
 * Class m200316_211025_table_phone_wokplaces
 */
class m200316_211025_table_phone_wokplaces extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('phone_wokplaces', [
            'id' => $this->primaryKey(),
            'phone' => $this->string()->comment('Телефон'),
            'wokplaces_id' => $this->string()->comment('Связь с филлиалом'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('phone_wokplaces');
    }
}
