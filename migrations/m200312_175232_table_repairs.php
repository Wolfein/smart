<?php

use yii\db\Migration;

/**
 * Class m200312_175232_table_repairs
 */
class m200312_175232_table_repairs extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('repairs', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('ФИО'),
            'phone' => $this->string()->comment('Телефон'),
            'doc' => $this->string()->comment('Документ'),
            'doc_info' => $this->string()->comment('Серия и номер'),
            'doc_who' => $this->string()->comment('Кем выдан'),
            'doc_date' => $this->string()->comment('Дата выдачи'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('repairs');
    }
}
