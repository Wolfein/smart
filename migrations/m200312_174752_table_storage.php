<?php

use yii\db\Migration;

/**
 * Class m200312_174752_table_storage
 */
class m200312_174752_table_storage extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('storage', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('storage');
    }
}
