<?php

use yii\db\Migration;

/**
 * Class m200312_165215_table_wokplaces
 */
class m200312_165215_table_wokplaces extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('wokplaces', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'city_id' => $this->integer()->comment('Город'),
            'address' => $this->string()->comment('Адреес'),
            'type_activity' => $this->string()->comment('Вариант ведения деятельности'),
            'entities_id' => $this->integer()->comment('Этот филиал работает через'),
            'photo' => $this->string()->comment('Фото'),
            'ser' => $this->string()->comment('Серия из настройкки печати'),
            'number' => $this->string()->comment('Номер следующего З/Б'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('wokplaces');
    }
}
