<?php

use yii\db\Migration;

/**
 * Class m200312_180126_table_entities
 */
class m200312_180126_table_entities extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('entities', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название юр. лица'),
            'address' => $this->string()->comment('Юридический адрес'),
            'phone' => $this->string()->comment('Общий телефон организации'),
            'director' => $this->string()->comment('Директор'),
            'buh' => $this->string()->comment('Главный бухгалтер'),
            'inn' => $this->string()->comment('ИНН'),
            'kpp' => $this->string()->comment('КПП'),
            'ogrn' => $this->string()->comment('ОГРН'),
            'okpo' => $this->string()->comment('ОКПО'),
            'oktmo' => $this->string()->comment('ОКТМО'),
        ]);

        $this->createTable('entities_bank', [
            'id' => $this->primaryKey(),
            'entities_id' => $this->integer()->comment('Связь'),
            'r_s' => $this->string()->comment('р/с'),
            'bank' => $this->string()->comment('Банк'),
            'k_s' => $this->string()->comment('к/с'),
            'bik' => $this->string()->comment('БИК'),
            'eye' => $this->boolean()->comment('Показ'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('entities');
        $this->dropTable('entities_bank');
    }
}
