<?php

use yii\db\Migration;

/**
 * Class m200312_175435_table_loyalty_cards
 */
class m200312_175435_table_loyalty_cards extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('loyalty_cards', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'buyout' => $this->float()->comment('Выкуп, %'),
            'extension' => $this->float()->comment('Продление, %'),
            'selected' => $this->float()->comment('Добор, %'),
            'part' => $this->float()->comment('Частичный выкуп, %'),
            'pay_acs' => $this->float()->comment('Скидка на покупку акссесуаров, %'),
            'real' => $this->float()->comment('Скидка на % с реализации товара, %'),
            'cash' => $this->float()->comment('КЭШБЕК, %'),
            'pay' => $this->float()->comment('Скидка при покупке товаров, %'),
            'pay_real' => $this->float()->comment('Скидка при покупке товаров, принятых на реализацию, %'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('loyalty_cards');
    }
}
