<?php

use yii\db\Migration;

/**
 * Class m200312_174900_table_categories
 */
class m200312_174900_table_categories extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'categories_id' => $this->integer()->comment('Родительская категория'),
            'tariff_id' => $this->integer()->comment('Тариф по умолчанию'),
            'storage_id' => $this->string()->comment('Место хранения по умолчанию'),
            'eye' => $this->boolean()->defaultValue(1)->comment('Показ'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('categories');
    }
}
