<?php

use yii\db\Migration;

/**
 * Class m200323_111940_table_operation_repairs
 */
class m200323_111940_table_operation_repairs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('operation_repairs', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'sn' => $this->string()->comment('Серийный номер'),
            'defect' => $this->string()->comment('Дефект'),
            'turn_device' => $this->boolean()->comment('Аппарат включается?'),
            'master_id' => $this->integer()->comment('Мастер'),
            'datetime_' => $this->string()->comment('Дата/время операции'),
            'user_id' => $this->string()->comment('Кто создал'),
            'status' => $this->integer()->comment('Статус'),
            'comment' => $this->text()->comment('Комментарий'),
        ]);

        $this->createTable('operation_repairs_photo', [
            'id' => $this->primaryKey(),
            'operation_repairs_id' => $this->integer()->comment('Cвязь'),
            'link' => $this->string()->comment('Тип документа'),
            'create_at' => $this->timestamp()->comment('Создан'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('operation_repairs');
        $this->dropTable('operation_repairs_photo');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200323_111940_table_operation_repairs cannot be reverted.\n";

        return false;
    }
    */
}
