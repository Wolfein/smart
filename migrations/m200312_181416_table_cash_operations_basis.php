<?php

use yii\db\Migration;

/**
 * Class m200312_181416_table_cash_operations_basis
 */
class m200312_181416_table_cash_operations_basis extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cash_operations_basis', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Основание кассовой/банковской операции'),
            'type' => $this->string()->comment('Вид'),
            'cas' => $this->string()->comment('Касса / Расчетный счет'),
            'eye' => $this->boolean()->defaultValue(1)->comment('Показ'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cash_operations_basis');
    }
}
