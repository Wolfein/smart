<?php

use yii\db\Migration;

/**
 * Class m200312_181959_table_templates
 */
class m200312_181959_table_templates extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('templates', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'type' => $this->string()->comment('Тип комис или ломбард'),
            'description' => $this->text()->comment('Описание'),
            'text' => $this->text()->comment('Текст'),
            'status' => $this->boolean()->comment('Статус'),
            'blank' => $this->boolean()->comment('Пустые бланки:'),
            'dep' => $this->boolean()->comment('Залог'),
            'contin' => $this->boolean()->comment('продление'),
            'contin_on' => $this->boolean()->comment('продление (online)'),
            'dep_rep' => $this->boolean()->comment('перезалог'),
            'dobor' => $this->boolean()->comment('добор'),
            'byuout_rem' => $this->boolean()->comment('частичный выкуп'),
            'byuout' => $this->boolean()->comment('выкуп'),
            'out' => $this->boolean()->comment('вывод из залога'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('templates');
    }
}
