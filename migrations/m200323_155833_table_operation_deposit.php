<?php

use yii\db\Migration;

/**
 * Class m200323_142226_table_operation_deposit
 */
class m200323_155833_table_operation_deposit extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('operation_deposit', [
            'id' => $this->primaryKey(),
            'ammount' => $this->float(),
            'r_s_id' => $this->integer()->comment('Касса / Расчетный счет'),
            'term_deposit' => $this->integer()->comment('Срок займа, дней'),
            'tariff_id' => $this->integer()->comment('Тариф'),
            'channels_id' => $this->integer()->comment('По какой рекламе пришли'),
            'comment' => $this->text()->comment('Комментарий'),
            'create_at' => $this->string()->comment('Создан'),
            'status' => $this->integer()->comment('Статус'),
            'user_id' => $this->string()->comment('Кто создал'),
        ]);

        $this->createTable('operation_deposit_subject', [
            'id' => $this->primaryKey(),
            'deposit_id' => $this->integer()->comment('Связь депозит'),
            'cat_id' => $this->integer()->comment('Категория'),
            'sub_cat_id' => $this->integer()->comment('Подкатегория'),
            'name' => $this->string()->comment('Наименование'),
            'ammount' => $this->float()->comment('Сумма оценки'),
            'sn' => $this->string()->comment('Серийный номер'),
            'storage_id' => $this->integer()->comment('Место хранения'),
            'description' => $this->text()->comment('Описание'),
            'status' => $this->integer()->comment('Статус'),
            'dt_status' => $this->string()->comment('Дата статуса'),
            'comment' => $this->text()->comment('Комментарий'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('operation_deposit');
        $this->dropTable('operation_deposit_subject');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200323_142226_table_operation_deposit cannot be reverted.\n";

        return false;
    }
    */
}
