<?php

use yii\db\Migration;

/**
 * Class m200312_181617_table_advertising_channels
 */
class m200312_181617_table_advertising_channels extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('advertising_channels', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'add_cl' => $this->boolean()->comment('Добавление клиента'),
            'dep' => $this->boolean()->comment('Залог'),
            'pay' => $this->boolean()->comment('Скупка'),
            'pay_self' => $this->boolean()->comment('Прием на реализацию'),
            'self' => $this->boolean()->comment('Продажа товара'),
            'self_acs' => $this->boolean()->comment('Продажа аксессуаров'),
            'add_rem' => $this->boolean()->comment('Прием на ремонт'),
            'eye' => $this->boolean()->defaultValue(1)->comment('Показ'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('advertising_channels');
    }
}
