<?php

use yii\db\Migration;

/**
 * Class m200323_132754_table_operation_accesories
 */
class m200323_132754_table_operation_accesories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('operation_accesories', [
            'id' => $this->primaryKey(),
            'cat_id' => $this->integer()->comment('Категория'),
            'name' => $this->string()->comment('Наименование'),
            'count' => $this->integer()->comment('Количество'),
            'ammount_purchase' => $this->float()->comment('Цена покупки 1'),
            'r_s_id' => $this->integer()->comment('Касса / Расчетный счет'),
            'ammount_sell' => $this->float( )->comment('Цена продажи 1'),
            'datetime_' => $this->string()->comment('Дата/время операции'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('operation_accesories');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200323_132754_table_operation_accesories cannot be reverted.\n";

        return false;
    }
    */
}
