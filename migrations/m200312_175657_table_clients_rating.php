<?php

use yii\db\Migration;

/**
 * Class m200312_175657_table_clients_rating
 */
class m200312_175657_table_clients_rating extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('clients_rating', [
            'id' => $this->primaryKey(),
            'deposit' => $this->float()->comment('Залог (только первичный, перезалоги не учитываются)'),
            'pay' => $this->float()->comment('Скупка'),
            'pay_real' => $this->float()->comment('Прием на реализацию'),
            'self' => $this->float()->comment('Продажа клиенту'),
            'outpay' => $this->float()->comment('Не выкуп (вывод из залога)'),
            'reserve' => $this->float()->comment('Изъятие (за каждую изъятую вещь)'),
        ]);


        $this->insert('clients_rating', [
            'deposit' => 1,
            'pay' => 1,
            'pay_real' => 1,
            'self' => 1,
            'outpay' => 1,
            'reserve' => 1,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('clients_rating');
    }
}
