<?php

use yii\db\Migration;

/**
 * Class m200312_175128_table_accessories
 */
class m200312_175128_table_accessories extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('accessories', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'accessories_id' => $this->integer()->comment('Родительская категория'),
            'eye' => $this->boolean()->defaultValue(1)->comment('Показ'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('accessories');
    }
}
