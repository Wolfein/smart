<?php

namespace app\validators;

use app\models\Param;
use yii\base\InvalidConfigException;
use yii\validators\Validator;

/**
 * Class ParamTypeValidator
 * @package app\validators
 */
class ParamTypeValidator extends Validator
{
    /**
     * @var int
     */
    public $type;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;

        $param = Param::findOne($value);

        if($param && $param->type != $this->type){
            $model->addError($attribute, 'Не корректный тип');
        }
    }
}