<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
//use app\models\OperationDeposit;
//use app\models\OperationDepositSearch;
use app\models\OperationDepositSubject;
use app\models\OperationDepositSubjectSearch;
use app\models\OperationRepairs;
use app\models\OperationRepairsSearch;
use app\models\OperationAccesories;
use app\models\OperationAccesoriesSearch;
use app\models\Operation;
use app\models\OperationSearch;
/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CatalogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [

        ];
    }

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = OperationDepositSubject::find();
        $searchModel = new OperationDepositSubjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('deposit', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionRepairs()
    {
        $query = OperationRepairs::find();
        $searchModel = new OperationRepairsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//            'pagination' => [
//                'pageSize' => 10,
//            ],
//            'sort' => [
//                'defaultOrder' => [
//                ]
//            ],
//        ]);
        return $this->render('repairs', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }


    public function actionAccesories()
    {
        $searchModel = new OperationAccesoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('accesories', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionBuying()
    {
        $searchModel = new OperationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('buying', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionDeposit()
    {
        $searchModel = new OperationDepositSubjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('deposit', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }


}
